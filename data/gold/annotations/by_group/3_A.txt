# ::id 25035
# ::text The baby is sleeping in the cradle.
(s / sleep-01
  :ARG0 (b / baby)
  :location (c / cradle))

# ::id 25036
# ::text The Mississippi River flows into the Gulf of Mexico.
(f / flow-01
  :ARG1 (s / river
    :wiki "Mississippi_River"
    :name (n / name
      :op1 "Mississippi"
      :op2 "River"))
  :direction (g / gulf
    :wiki "Gulf_of_Mexico"
    :name (n2 / name
      :op1 "Gulf"
      :op2 "of"
      :op3 "Mexico")))

# ::id 25037
# ::text Tom has dyed his hair black.
(d / dye-01
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG1 (h / hair
    :poss p)
  :ARG2 (b / black-04))

# ::id 25038
# ::text He's a very capable business man.
(b / business_man
  :ARG1-of (c / capable-01
    :degree (v / very))
  :domain ( h/ he))

# ::id 25039
# ::text "What group sang the song ""Happy Together""?"
(s / sing
  :ARG0 (a2 / amr-unknown
    :domain (g2 / group))
  :ARG1 (s2 / song
          :wiki "Happy_Together_(song)"
          :name (n / name
               :op1 "Happy"
               :op2 "Together")))

# ::id 25040
# ::text She roasted the turkey.
(r / roast-01
  :ARG0 (s / she)
  :ARG1 (t / turkey))

# ::id 25041
# ::text Tom suspected that his father was dyslexic.
(s / suspect-01
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG1 (d / dyslexic
    :domain (p2 / person
     :ARG0-of (h / have-rel-role-91
          :ARG1 (h2 / he)
          :ARG2 (f / father)))))

# ::id 25042
# ::text I am acquainted with Mr Smith.
(a / acquaint-01
  :ARG0 (i / i)
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Mr."
      :op2 "Smith")))


# ::id 25043
# ::text Which suitcases are Tom's?
# Note inspired by lpp_1943.122
(s / suitcase
      :poss (p / person
        :wiki -
        :name (n / name
          :op1 "Tom"))
      :domain (a / amr-unknown))

# ::id 25044
# ::text She offered me a beer.
(o / offer-01
  :ARG0 (s / she)
  :ARG1 (b / beer)
  :ARG3 (i / i))

# ::id 25045
# ::text I am from Ecuador.
(c / come-01
  :ARG1 (i / i)
  :ARG3 (c2 / country
    :wiki "Ecuador"
    :name (n / name
      :op1 "Ecuador")))

# ::id 25046
# ::text I was talking to him.
(t / talk-01
  :ARG0 (i / i)
  :ARG1 (h / he))

# ::id 25047
# ::text I showered before breakfast.
(s / shower-01
  :ARG0 (i / i)
  :time (b / before
    :op1 (b2 / breakfast-01)))

# ::id 25048
# ::text The park is located in the center of the city.
# NOTE: there are concepts "center-xx" but with very different senses.
(b / be-located-at-91
  :ARG1 (p / park)
  :ARG2 (c / center
    :part-of (c2 / city)))

# ::id 25049
# ::text Tom was very jealous.
# Note : cannot decide jealous-01 VS jealous-02 without more context
(j / jealous-01
  :degree (v / very)
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom")))

# ::id 25050
# ::text I am boiling water.
(b / boil-01
  :ARG0 (i / i)
  :ARG1 (w / water))

# ::id 25051
# ::text He told me an interesting story.
(t / tell-01
  :ARG0 (h / he)
  :ARG1 (s / story
    :ARG0-of (i / interest-01))
  :ARG2 (i2 / i))
