# ::id 25001
# ::text Many a quarrel comes about through a misunderstanding.
(c / come-about-06
  :ARG1 (q / quarrel-01
    :quant (m / many))
  :ARG2 (t / thing
    :ARG1-of (m2 / misunderstand-01)))

# ::id 25002
# ::text She sobbed heavily.
# Note heavy-01 not in https://amr.isi.edu/doc/propbank-amr-frames-arg-descr.txt
(s / sob-01
  :ARG0 (s2 / she)
  :ARG1-of (h / heavy-01))

# ::id 25003
# ::text That was incredible, wasn't it?
# Note: Cannot find any frame credible ou incredible
(i / incredible
  :domain (t / that)
  :ARG1-of (r / request-confirmation-91))

# ::id 25004
# ::text The rope broke under the strain.
# Note: interesting case of incohative "Tom broke the rope VS the rope broke"
# Note: another option strain as the instrument? It would be ARG2
(b / break-01
  :ARG1 (r / rope)
  :cause (s / strain))

# ::id 25005
# ::text I love pizza.
(l / love-01
  :ARG0 (i / i)
  :ARG1 (p / pizza))

# ::id 25006
# ::text Mr Joel is now on duty.
(o / on-duty
  :domain (p / person
    :wiki -
    :name (n / name
      :op1 "Mr"
      :op2 "Joel"))
  :time (n8 / now))

# ::id 25007
# ::text The wounded were transported with an ambulance.
(t / transport-01
  :ARG1 (p /person
    :ARG1-of (w / wound-01))
  :manner (a / ambulance))

# ::id 25008
# ::text He asked me whether I like math.
(a / ask-01 
  :ARG0 (h / he) 
  :ARG1 (t / truth-value 
    :polarity-of (l / like-01 
      :ARG0 i
      :ARG1 (m / math)))
  :ARG2 (i / i))

# ::id 25009
# ::text Mary wrapped herself in a towel.
(w / wrap-00
  :ARG0 (p / person
    :name (n / name
      :op1 "Mary"))
  :ARG1 p
  :location (t / towel))

# ::id 25010
# ::text He looked like a doctor.
(l / look-02
  :ARG0 (h / he)
  :ARG1 (d / doctor))

# ::id 25011
# ::text A cold bath refreshed him.
(r / refresh-01
  :ARG0 (b / bath
    :ARG1-of (c / cold-01))
  :ARG1 (h / he))

# ::id 25012
# ::text That cut looks infected.
(l / look-02
  :ARG0 (t / thing
    :mod (t2 / that)
    :ARG1-of (c / cut-01))
  :ARG1 (t3 / thing
    :ARG1-of (i / infect-01)))

# ::id 25013
# ::text Liisa was sick of the noise.
(s / sick-05
  :ARG1 (p / person
    :name (n / name
      :op1 "Liisa"))
    :ARG1-of (c / cause-01 
      :ARG0 (r / noise)))

# ::id 25014
# ::text He ran away from home.
(r / run-02
  :ARG0 (h / he) 
    :direction (a / away 
      :op1 (h / home)))

# ::id 25015
# ::text My little sister looks like my mum.
(l / look-02
  :ARG0 (s / sister
    :poss (i / i))
  :ARG1 (m / mother
    :poss i))

# ::id 25016
# ::text What city did Duke Ellington live in?
(l / live-01
  :AGRG0 (p / person
          :wiki "Duke_Ellington"
          :name (n / name
               :op1 "Duke"
               :op2 "Ellington"))
  :location (c / city
    :name (n3 / name
      :op1 (a / amr-unknown))))

# ::id 25017
# ::text The bear ate an apple.
(e / eat-01 
  :ARG0 (b / bear) 
  :ARG1 (a / apple))

# ::id 25018
# ::text Tom is strong.
(s / strong-02 
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom")))

# ::id 25019
# ::text I like picnics.
(l / like-01
  :ARG0 (i / i)
  :ARG1 (p / picnic))

# ::id 25020
# ::text The red skirt is new.
(n / new-01 
  :ARG1 (s / skirt
  :ARG1-of (w / red-02)))

# ::id 25021
# ::text She's wearing a nice hat.
(l / wear-01
  :ARG0 (s / she)
  :ARG1 (h / hat
    :ARG1-of (w / nice-01)))

# ::id 25022
# ::text I won!
(w / win-01
  :ARG0 (i / i))

# ::id 25023
# ::text She is French.
(s / she
  :mod (c / country
    :wiki "France"
    :name (n / name
      :op1 "France")))

# ::id 25024
# ::text I owe her thirty dollars.
(o / owe-01
  :ARG0 (i / i)
  :ARG1 (q / monetary-quantity
    :quant 30
    :unit (d / dollar))
  :ARG2 (s / she))

# ::id 25025
# ::text The novels he wrote are interesting.
(i / interest-01
  :ARG0 (n / novel
    :ARG1-of (w / write-01
      :ARG0 (h / he))))

# ::id 25026
# ::text I don't speak Hebrew.
(s / speak-01
  :polarity -
  :ARG0 (i / i)
  :ARG3 (l / language 
    :wiki "Hebrew" 
    :name (n3 / name 
      :op1 "Hebrew")))


# ::id 25027
# ::text What does the abbreviation IRA stand for?
(s / stand-08
  :ARG0 (c / abbreviation
    :wiki Irish_Republican_Army
    :name (n / name
      :op1 "IRA"))
  :ARG1 (a / amr-unknown))

# ::id 25028
# ::text I always get up at 6 o'clock in the morning.
(g /get-up-26
  :ARG1 (i / i)
  :time (a / always)
  :time (d / date-entity 
     :time "6:00"))

# ::id 25029
# ::text I can't beat Tom at chess.
(p / possible-01 
  :polarity - 
  :ARG1 (b / beat-03
    :ARG0 (i / i) 
    :ARG1 (p2 / person
      :wiki -
      :name (n / name
        :op1 "Tom"))
    :ARG2 (c / chess)))

# ::id 25030
# ::text Where did you guys meet?
# NOTE: collective verb
(m / meet-03
  :ARG0 (g / guy)
  :location (a / amr-unknown))

# ::id 25031
# ::text I'm always bored with his boastful talk.
(b / bore-02
  :ARG0 (b2 / boast-01
    :ARG0 (h / he)
    :ARG1 (t / talk-01
      :ARG0 h))
  :ARG1 (i /i)
  :time (a / always))

# ::id 25032
# ::text The Normandy landings took place in June 1944.
(h / land-01
  :wiki "Normandy_landings"
  :name (n / name
    :op1 "Normandy"
    :op2 "landings")
  :time (d / date-entity
    :month 6
    :year 1944
    :calendar (j / julian)))

# ::id 25033
# ::text Tom was surprised the police knew his name.
(s / surprise-01
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG0 (k / know-01
    :ARG0 (p2 / police)
    :ARG1 (n2/ name
      :poss p)))

# ::id 25034
# ::text The oil pipeline is leaking.
(l / leak-01
  :ARG0 (p / pipeline
    :mod (o / oil)))

# ::id 25035
# ::text The baby is sleeping in the cradle.
(s / sleep-01
  :ARG0 (b / baby)
  :location (c / cradle))

# ::id 25036
# ::text The Mississippi River flows into the Gulf of Mexico.
(f / flow-01
  :ARG1 (s / river
    :wiki "Mississippi_River"
    :name (n / name
      :op1 "Mississippi"
      :op2 "River"))
  :direction (g / gulf
    :wiki "Gulf_of_Mexico"
    :name (n2 / name
      :op1 "Gulf"
      :op2 "of"
      :op3 "Mexico")))

# ::id 25037
# ::text Tom has dyed his hair black.
(d / dye-01
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG1 (h / hair
    :poss p)
  :ARG2 (b / black-04))

# ::id 25038
# ::text He's a very capable business man.
(b / business_man
  :ARG1-of (c / capable-01
    :degree (v / very))
  :domain ( h/ he))

# ::id 25039
# ::text "What group sang the song ""Happy Together""?"
(s / sing
  :ARG0 (a2 / amr-unknown
    :domain (g2 / group))
  :ARG1 (s2 / song
          :wiki "Happy_Together_(song)"
          :name (n / name
               :op1 "Happy"
               :op2 "Together")))

# ::id 25040
# ::text She roasted the turkey.
(r / roast-01
  :ARG0 (s / she)
  :ARG1 (t / turkey))

# ::id 25041
# ::text Tom suspected that his father was dyslexic.
(s / suspect-01
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG1 (d / dyslexic
    :domain (p2 / person
     :ARG0-of (h / have-rel-role-91
          :ARG1 (h2 / he)
          :ARG2 (f / father)))))

# ::id 25042
# ::text I am acquainted with Mr Smith.
(a / acquaint-01
  :ARG0 (i / i)
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Mr."
      :op2 "Smith")))


# ::id 25043
# ::text Which suitcases are Tom's?
# Note inspired by lpp_1943.122
(s / suitcase
      :poss (p / person
        :wiki -
        :name (n / name
          :op1 "Tom"))
      :domain (a / amr-unknown))

# ::id 25044
# ::text She offered me a beer.
(o / offer-01
  :ARG0 (s / she)
  :ARG1 (b / beer)
  :ARG3 (i / i))

# ::id 25045
# ::text I am from Ecuador.
(c / come-01
  :ARG1 (i / i)
  :ARG3 (c2 / country
    :wiki "Ecuador"
    :name (n / name 
      :op1 "Ecuador")))

# ::id 25046
# ::text I was talking to him.
(t / talk-01
  :ARG0 (i / i)
  :ARG1 (h / he))

# ::id 25047
# ::text I showered before breakfast.
(s / shower-01
  :ARG0 (i / i)
  :time (b / before
    :op1 (b2 / breakfast-01)))

# ::id 25048
# ::text The park is located in the center of the city.
# NOTE: there are concepts "center-xx" but with very different senses.
(b / be-located-at-91
  :ARG1 (p / park)
  :ARG2 (c / center
    :part-of (c2 / city)))

# ::id 25049
# ::text Tom was very jealous.
# Note : cannot decide jealous-01 VS jealous-02 without more context
(j / jealous-01
  :degree (v / very) 
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom")))

# ::id 25050
# ::text I am boiling water.
(b / boil-01
  :ARG0 (i / i)
  :ARG1 (w / water))

# ::id 25051
# ::text He told me an interesting story.
(t / tell-01
  :ARG0 (h / he)
  :ARG1 (s / story
    :ARG0-of (i / interest-01))
  :ARG2 (i2 / i))

