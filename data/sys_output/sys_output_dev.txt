# ::id 00001
# ::text I began playing golf years ago.
(b / begin-01
	:part-of (p / play-01
		:ARG1 (g / golf)
		:ARG0 (i / i))
	:time (t2 / time
		:TPR now
		:TAB (t / temporal-quantity
			:TAB now
			:unit (y / year))))

# ::id 00002
# ::text I cooked him dinner.
(c / cook-01
	:ARG1 (d / dinner)
	:beneficiary (h / he)
	:ARG0 (i / i))

# ::id 00003
# ::text Aren't those your parents?
(b / be-01
	:polarity -
	:ARG2 (h / have-rel-role-91
		:ARG1 (y / you)
		:ARG2 (p / parent)
		:ARG0 (p2 / person))
	:ARG1 (p3 / person))

# ::id 00004
# ::text I bit my tongue.
(b / bite-01
	:ARG1 (t / tongue
		:part-of (i / i))
	:ARG0 i)

# ::id 00005
# ::text The matches cost ten pennies.
(c / cost-01
	:ARG2 (m / monetary-quantity
		:quant 10
		:unit (p / penny))
	:ARG1 (m2 / match))

# ::id 00006
# ::text I kind of liked Tom.
(l / like-01
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom"))
	:manner (k / kind-of)
	:ARG0 (i / i))

# ::id 00007
# ::text I think my girlfriend is kind of cute.
(t / think-01
	:ARG1 (c / cute
		:domain (h / have-rel-role-91
			:ARG2 (g / girlfriend)
			:ARG0 (p / person)
			:ARG1 (i / i))
		:degree (k / kind-of))
	:ARG0 i)

# ::id 00008
# ::text He is Japanese.
(b / be-01
	:source (c / country
		:wiki "japan"
		:name (n / name
			:op1 "japan"))
	:ARG2 (h / he))

# ::id 00009
# ::text Do you speak Hebrew?
(s / speak-01
	:ARG3 (h / hebrew)
	:ARG0 (y / you))

# ::id 00010
# ::text We must destroy the evidence.
(o / obligate-01
	:ARG2 (d / destroy-01
		:ARG1 (e / evidence)
		:ARG0 (w / we)))

# ::id 00011
# ::text I also lost my cell phone!
(l / lose-02
	:ARG1 (c / cellphone
		:poss (i / i))
	:ARG0 i)

# ::id 00012
# ::text She has a rare given name.
(h / have-03
	:ARG1 (g / given-name
		:mod (r / rare))
	:ARG0 (s / she))

# ::id 00013
# ::text Tom punched Mary in the stomach.
(p2 / punch-01
	:location (s / stomach)
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Mary"))
	:ARG0 (p3 / person
		:wiki -
		:name (n2 / name
			:op1 "Tom")))

# ::id 00014
# ::text He was attacked by a shark.
(a / attack-01
	:ARG0 (s / shark)
	:ARG1 (h / he))

# ::id 00015
# ::text I apologize. It was my mistake.
(m / multi-sentence
	:snt (b / be-01
		:ARG2 (m2 / mistake
			:Creator (i / i))
		:ARG1 (e / entity))
	:snt1 (a / apologize-01
		:ARG0 i))

# ::id 00016
# ::text Tom chickened out.
(c / chicken-out
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00017
# ::text There are always a lot of vehicles on this road.
(b / be-located-at-91
	:ARG2 (r / road)
	:ARG1 (v / vehicle
		:quant (q / quantity
			:EQU +)))

# ::id 00018
# ::text Tom told me that he'd lost his watch.
(t / tell-01
	:ARG1 (l / lose-02
		:ARG1 (w / watch
			:poss (p / person
				:wiki -
				:name (n / name
					:op1 "Tom")))
		:ARG0 p)
	:ARG2 (i / i)
	:ARG0 p)

# ::id 00019
# ::text The appendix is about 10 cm long.
(l / long
	:ARG2 (a / about
		:op1 (d / distance-quantity
			:quant 10
			:unit (c / cm)))
	:ARG1 (a2 / appendix))

# ::id 00020
# ::text He drank beer.
(d / drink-01
	:ARG1 (b / beer)
	:ARG0 (h / he))

# ::id 00021
# ::text Tom seems worried.
(s / seem-01
	:ARG1 (w / worried
		:domain (p / person
			:wiki -
			:name (n / name
				:op1 "Tom")))
	:ARG1 p)

# ::id 00022
# ::text Leo Tolstoy was a vegetarian.
(v / vegetarian
	:domain (p / person
		:wiki -
		:name (n / name
			:op1 "Leo"
			:op2 "Tolstoy")))

# ::id 00023
# ::text Who sang "Tennessee Waltz"?
(s / sing-01
	:ARG1 (m / music
		:wiki "Tennessee Waltz"
		:name (n / name
			:op1 "Tennessee"
			:op2 "Waltz"))
	:ARG0 (a / amr-unknown))

# ::id 00024
# ::text They are holding it.
(h / hold-01
	:ARG1 (e / entity)
	:ARG0 (p / person))

# ::id 00025
# ::text Her favourite band is Warpaint.
(b / be-01
	:ARG2 (m / musical-organization
		:wiki "Warpaint"
		:name (n / name
			:op1 "Warpaint"))
	:ARG1 (b2 / band))

# ::id 00026
# ::text Dick played piano and Lucy sang.
(a / and
	:op2 (s / sing-01
		:ARG0 (p / person
			:wiki -
			:name (n / name
				:op1 "Lucy")))
	:op1 (p3 / play-01
		:ARG1 (p2 / piano)
		:ARG0 (p4 / person
			:wiki -
			:name (n2 / name
				:op1 "Dick"))))

# ::id 00027
# ::text He's dark and handsome.
(h2 / he
	:mod (h / handsome)
	:mod (d / dark))

# ::id 00028
# ::text This television set is heavy.
(h / heavy
	:ARG2 +
	:mod (t / television-set))

# ::id 00029
# ::text I took two aspirin for my headache.
(t / take-01
	:ARG2 (h / headache
		:ARG0 (i / i))
	:ARG1 (a / aspirin
		:quant 2)
	:ARG0 i)

# ::id 00030
# ::text Tom is paying a fine.
(p / pay-01
	:ARG3 (f / fine)
	:ARG0 (p2 / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00031
# ::text A flute is being played by a girl
(p / play-01
	:ARG0 (g / girl)
	:ARG1 (f / flute))

# ::id 00032
# ::text European cars sell in Russia.
(s / sell-01
	:location (c / country
		:wiki "Russia"
		:name (n / name
			:op1 "Russia"))
	:ARG1 (c2 / car
		:source (c3 / continent
			:wiki "europe"
			:name (n2 / name
				:op1 "europe"))))

# ::id 00033
# ::text Tom is manipulative.
(m / manipulative
	:domain (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00034
# ::text Maybe Tom killed himself.
(p / possible-01
	:ARG1 (k / kill-01
		:ARG1 (p2 / person
			:wiki -
			:name (n / name
				:op1 "Tom"))
		:ARG0 p2))

# ::id 00035
# ::text Whose is this camera?
(b / be-01
	:ARG1 (c / camera)
	:ARG2 (w / we))

# ::id 00036
# ::text The game excited lots of people.
(e / excite-01
	:ARG1 (p / person
		:quant (q / quantity
			:EQU +))
	:ARG0 (g / game))

# ::id 00037
# ::text He cooked me dinner.
(c / cook-01
	:ARG1 (d / dinner)
	:beneficiary (i / i)
	:ARG0 (h / he))

# ::id 00038
# ::text Tom is extremely unfriendly.
(u / unfriendly
	:degree (e / extremely)
	:mod (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00039
# ::text There were many rotten apples in the basket.
(b2 / be-located-at-91
	:ARG2 (b / basket)
	:ARG1 (a / apple
		:quant (m / many)
		:mod (r / rotten)))

# ::id 00040
# ::text The girl lacked musical ability.
(l / lack-01
	:ARG1 (a / ability
		:mod (m / musical))
	:ARG0 (g / girl))

# ::id 00041
# ::text This car is bigger than that one.
(h / have-degree-91
	:ARG4 (c / car)
	:ARG2 (m / more)
	:ARG3 (b / big)
	:ARG1 (c2 / car))

# ::id 00042
# ::text I am convinced that he is innocent.
(c / convinced
	:ARG1 (i / innocent
		:domain (h / he))
	:ARG0 (w / we))

# ::id 00043
# ::text Can you turn on the air conditioning?
(p / possible-01
	:ARG1 (t / turn-on
		:ARG1 (a / air-conditioning)
		:ARG0 (y / you)))

# ::id 00044
# ::text He is named Jeff by everyone.
(n2 / name-02
	:ARG0 (p / person)
	:ARG2 (n / name
		:EQU "Jeff")
	:ARG1 (h / he))

# ::id 00045
# ::text Tom took his finger off the trigger.
(t2 / take-off-07
	:source (t / trigger)
	:ARG1 (f / finger
		:part-of (p / person
			:wiki -
			:name (n / name
				:op1 "Tom")))
	:ARG0 p)

# ::id 00046
# ::text Name a poem by Eugenio Montale.
(n2 / name-01
	:ARG1 (p2 / poem
		:Creator (p / person
			:wiki -
			:name (n / name
				:op1 "Eugenio"
				:op2 "Montale")))
	:ARG0 (y / you))

# ::id 00047
# ::text He tried to seduce me.
(t / try-01
	:ARG1 (s / seduce-01
		:ARG1 (i / i)
		:ARG0 (h / he))
	:ARG0 h)

# ::id 00048
# ::text His house is very far from the station.
(f / far
	:ARG1 (s / station)
	:degree (v / very)
	:mod (h / house
		:poss (h2 / he)))

# ::id 00049
# ::text I abhor spiders.
(a / abhor
	:ARG1 (s / spider)
	:ARG0 (i / i))

# ::id 00050
# ::text A cow is eating hay
(e / eat-01
	:ARG1 (h / hay)
	:ARG0 (c / cow))

# ::id 00051
# ::text She accompanied me on the piano.
(a / accompany
	:instrument (p / piano)
	:ARG1 (i / i)
	:ARG0 (s / she))

# ::id 00052
# ::text The trees are green.
(g / green
	:ColourOf (t / tree))

# ::id 00053
# ::text How did Harry Houdini die?
(d / die-01
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Harry"
			:op2 "Houdini"))
	:manner (a / amr-unknown))

# ::id 00054
# ::text A mile is equal to about 1600 meters.
(e / equal
	:ARG2 (a / about
		:op1 (d / distance-quantity
			:quant 1600
			:unit (m / meter)))
	:ARG1 (d2 / distance-quantity
		:quant 1
		:unit (m2 / mile)))

# ::id 00055
# ::text Tom unbuckled his seatbelt.
(u / unbuckle
	:ARG1 (s / seatbelt
		:poss (p / person
			:wiki -
			:name (n / name
				:op1 "Tom")))
	:ARG0 p)

# ::id 00056
# ::text He gave me a half dozen linen handkerchiefs.
(g / give-01
	:ARG1 (h / handkerchief
		:quant 6
		:mod (l / linen))
	:ARG2 (i / i)
	:ARG0 (h2 / he))

# ::id 00057
# ::text The old lady smiled at her granddaughter.
(s / smile-02
	:ARG2 (h / have-rel-role-91
		:ARG2 (g / granddaughter)
		:ARG1 (p / person
			:EQU (f / female))
		:EQU f
		:ARG0 (l / lady
			:mod (o / old
				:ARG2 +)))
	:ARG0 h)

# ::id 00058
# ::text This squid is five quids.
(b / be-01
	:ARG2 (m / monetary-quantity
		:quant 5
		:unit (q / quid))
	:ARG1 (s / squid))

# ::id 00059
# ::text "I was a boy," he said impatiently.
(s / say-01
	:ARG1 (b / boy
		:domain (w / we))
	:manner (i / impatiently)
	:ARG0 (h / he))

# ::id 00060
# ::text How many people survived the Auschwitz concentration camp?
(s / survive-01
	:location (c / concentration-camp
		:wiki "Auschwitz"
		:name (n / name
			:op1 "Auschwitz"))
	:ARG0 (p / person
		:quant (a / amr-unknown)))

# ::id 00061
# ::text The boys set up a hockey rink on the frozen pond.
(s / set-up
	:ARG2 (i / ice-hockey-rink
		:location (p / pond
			:mod (f / frozen)))
	:ARG0 (b / boy))

# ::id 00062
# ::text I'm not outgoing.
(o / outgoing
	:polarity -
	:ARG0 (i / i))

# ::id 00063
# ::text Tom waved at Mary.
(w / wave-01
	:ARG2 (p / person
		:wiki -
		:name (n / name
			:op1 "Mary"))
	:ARG0 (p2 / person
		:wiki -
		:name (n2 / name
			:op1 "Tom")))

# ::id 00064
# ::text I grabbed the cat by the neck.
(g / grab-01
	:ARG1 (n / neck)
	:ARG1 (c / cat)
	:ARG0 (i / i))

# ::id 00065
# ::text A parrot can mimic a person's voice.
(p / possible-01
	:ARG1 (m / mimic-01
		:ARG1 (v / voice
			:Creator (p2 / person))
		:ARG0 (p3 / parrot)))

# ::id 00066
# ::text The pen has run out of ink.
(r / run-out
	:ARG1 (i / ink)
	:source (p / pen))

# ::id 00067
# ::text I'm at the airport.
(b / be-located-at-91
	:ARG2 (a / airport)
	:ARG1 (i / i))

# ::id 00068
# ::text Tom is trapped.
(t / trap-01
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00069
# ::text It snowed for ten consecutive days.
(s / snow-01
	:duration (t / temporal-quantity
		:quant 10
		:unit (d / day)
		:mod (c / consecutive)))

# ::id 00070
# ::text Tom is an excellent tennis player.
(t / tennis-player
	:mod (e / excellent)
	:domain (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00071
# ::text I'm an alcoholic. I'm a drug addict. I'm homosexual. I'm a genius.
(m / multi-sentence
	:snt (g / genius
		:domain (i / i))
	:snt (h / homosexual
		:domain i)
	:snt (d / drug-addict
		:domain i)
	:snt1 (a / alcoholic
		:domain i))

# ::id 00072
# ::text I can't find the newspaper.
(p / possible-01
	:polarity -
	:ARG1 (f / find-01
		:ARG1 (n / newspaper)
		:ARG0 (i / i)))

# ::id 00073
# ::text He weighs 70 kilos.
(w / weigh-01
	:ARG2 (m / mass-quantity
		:quant 70
		:unit (k / kilo))
	:ARG1 (h / he))

# ::id 00074
# ::text Prostitution is legal in Germany.
(l / legal
	:location (c / country
		:wiki "Germany"
		:name (n / name
			:op1 "Germany"))
	:mod (p / prostitution))

# ::id 00075
# ::text Everyone said that I was wrong.
(s / say-01
	:ARG1 (w / wrong
		:domain (i / i))
	:ARG0 (p / person))

# ::id 00076
# ::text What scared you?
(s / scare-01
	:ARG0 (a / amr-unknown)
	:ARG1 (y / you))

# ::id 00077
# ::text His slacks are all wrinkled.
(w / wrinkled
	:mod (e / entity
		:SubOf (s / slacks
			:poss (h / he))))

# ::id 00078
# ::text Mary combed her fingers through her hair.
(c / comb
	:ARG1 (h / hair
		:part-of (p / person
			:wiki -
			:name (n / name
				:op1 "Mary")))
	:ARG0 p
	:instrument (f / finger
		:part-of p))

# ::id 00079
# ::text I knew it was unhealthy.
(k / know-01
	:ARG1 (u / unhealthy
		:domain (e / entity))
	:ARG0 (i / i))

# ::id 00080
# ::text Harnold Lamb authored many biographies.
(a / author
	:ARG2 (b / biography
		:quant (m / many))
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Harnold"
			:op2 "Lamb")))

# ::id 00081
# ::text She bit him.
(b / bite-01
	:ARG1 (h / he)
	:ARG0 (s / she))

# ::id 00082
# ::text The clerk labeled the baggage.
(l / label-01
	:ARG1 (b / baggage)
	:ARG0 (c / clerk))

# ::id 00083
# ::text I put a few blueberries in your tea.
(p / put-01
	:ARG2 (t / tea
		:poss (y / you))
	:ARG1 (b / blueberry
		:quant -)
	:ARG0 (i / i))

# ::id 00084
# ::text This expedition will be expensive.
(e / expensive
	:time (t / time
		:TSU now)
	:mod (e2 / expedition))

# ::id 00085
# ::text Every wall is a door.
(d / door
	:domain (w / wall))

# ::id 00086
# ::text I am a tennis player.
(t / tennis-player
	:domain (i / i))

# ::id 00087
# ::text None of the cars is mine.
(b / be-01
	:polarity -
	:ARG2 (e / entity
		:poss (i / i))
	:ARG1 (e2 / entity
		:SubOf (c / car)))

# ::id 00088
# ::text There's a hole in this sock.
(b / be-located-at-91
	:ARG2 (s / sock)
	:ARG1 (h / hole))

# ::id 00089
# ::text Yedinstvo.
(e / event
	:Participant (p / party
		:wiki "Yedinstvo"
		:name (n / name
			:op1 "Yedinstvo")))

# ::id 00090
# ::text Tom is skinny.
(s / skinny
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 00091
# ::text My car is being repaired.
(r / repair-01
	:ARG1 (c / car
		:poss (i / i)))

# ::id 00092
# ::text Tom now lives in Boston.
(l / live-01
	:location (c / city
		:wiki "Boston"
		:name (n / name
			:op1 "Boston"))
	:ARG0 (p / person
		:wiki -
		:name (n2 / name
			:op1 "Tom")))

# ::id 00093
# ::text There is a pen on the desk.
(b / be-located-at-91
	:ARG2 (d / desk)
	:ARG1 (p / pen))

# ::id 00094
# ::text This afternoon, Becker lost to Austrian Gilbert Schaller (2:6, 2:6).
(l / lose-03
	:extent (e / entity
		:Sub (s / score
			:ARG2 2-6)
		:Sub (s2 / score
			:ARG2 2-6))
	:ARG2 (p / person
		:wiki -
		:name (n / name
			:op1 "Gilbert"
			:op2 "Schaller")
		:source (c / country
			:wiki "austria"
			:name (n2 / name
				:op1 "austria")))
	:ARG0 (p2 / person
		:wiki -
		:name (n3 / name
			:op1 "Becker"))
	:time (a / afternoon))

# ::id 00095
# ::text Emily is afraid of spiders.
(a / afraid
	:ARG1 (s / spider)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Emily")))

