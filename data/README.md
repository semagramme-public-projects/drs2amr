## Annotation procedure

We have used the following resources and tools:

* The [AMR guidelines](https://github.com/amrisi/amr-guidelines/blob/master/amr.md)
as a primary source for examples and details on how to annotate
different phenomena (as of 02 Mar 2022, this link points to version
1.2.6 of the guidelines and has been last modified on 18 Mar 2021)
* The [AMR Annotation Dictionary](https://www.isi.edu/~ulf/amr/lib/amr-dict.html)
for examples grouped by specific roles, concepts, words and constructions (on [Grew-match](http://semantics.grew.fr/?corpus=AMR_dict))
* For predicate senses and predicate-argument structure we use the [PropBank](http://propbank.github.io/).
  * Searchable frame file are [here](http://verbs.colorado.edu/propbank/framesets-english-aliases/). The problem with this list is that some frames are difficult to find: for instance the frame `strong-01` is "hidden" in the page [`strengthen.html`](http://verbs.colorado.edu/propbank/framesets-english-aliases/strengthen.html).
  * A full [list of PropBank frames on AMR website](https://amr.isi.edu/doc/propbank-amr-frames-arg-descr.txt) seems easier to use in practise.
* You can use [Grew Transform](http://transform.grew.fr/) to verify there
are no syntax errors and to visualise your annotations

### Expected annotation format

Either [PENMAN notation](https://penman.readthedocs.io/en/latest/notation.html)
or [CoNLL](https://universaldependencies.org/format.html) is fine.

For either notation, make sure to add the **sentence ID** and **text**
as comments above the sentence:

```
# ::id w01058013
# ::text The last Olympic Games are believed to have been held in 393.
anotation here
```

**NB**: there **must not** be an empty line between the metadata
and the start of the annotation.

Put all the annotations in the same file, with one empty line between
sentences. E.g.:

```
# ::id 0
# ::text Here is the first sentence.
anotation

# ::id 1
# ::text Here is another sentence.
annotation 
```

#### PENMAN notation example

```
# ::id w01058013
# ::text The last Olympic Games are believed to have been held in 393.
(b / believe-01
    :ARG1 (g / game
        :mod (l / last)
        :wiki "Ancient_Olympic_Games"
        :name (n /name
            :op1 "Olympic"
            :op2 "Games"))
    :ARG2 (h / hold-04
        :ARG1 g
        :time (d / date-entity
            :year 393)))
```

Tabulation and new lines are not strictly necessary, but help
with readability.

## *test* set (GOLD)

#### Annotation split

The 102 sentences that we have selected from the PMB for
annotation are split into six groups (1-6) of 17 sentences
to ensure each pair of annotators has the same number of
overlapping sentences. We had four annotators - A, B, C and D.
Each sentence was annotated by two different annotators.
The table below shows the annotators per group.

| Group # | A   | B   | C   | D   | 
|---------|-----|-----|-----|-----|
| 1       | x   | x   |     |     |
| 2       | x   |     | x   |     |
| 3       | x   |     |     | x   |
| 4       |     | x   | x   |     |
| 5       |     | x   |     | x   |
| 6       |     |     | x   | x   |

#### Raw data and annotations

The raw data can be found in `/gold/raw_data/`.
The folder contains a split by annotator and by group in `/by_annotator/`
and `/by_group/` respectively.

The annotations by each annotator are in `/gold/annotations/`.
Again, the folder contains a split by annotator and by group in `/by_annotator/`
and `/by_group/` respectively.

The final **gold** annotations are in `/gold/gold.txt`. For each sentence
where we did not have full agreement, the gold annotation was decided
on by the annotators during a few discussion sessions as described in the paper.
The reasoning behind each decision is in `/gold/choice_decisions.txt`.

A key to PMB IDs can be found in `/gold/gold_to_pmb_key.csv`.

#### Inter-annotator agreement

We have used [Smatch](https://github.com/snowblink14/smatch) to calculate the
inter-annotator agreement. The Smatch F1-score is as follows:

|     | A    | B    | C    | D    | 
|-----|------|------|------|------|
| A   | -    | 0.76 | 0.82 | 0.81 |
| B   | 0.76 | -    | 0.83 | 0.86 |
| C   | 0.82 | 0.83 | -    | 0.84 |
| D   | 0.81 | 0.86 | 0.84 | -    |

Smatch scores per annotation pair per sentence can be found in `/gold/iaa_granular.md`.

## *dev* set

All 95 sentences in the *dev* set were annotated by annotator D.
The annotations can be found in `/dev/dev.txt`. An ID mapping to PMB IDs
can be found in `/dev/dev_to_pmb_key.txt`.

## System output

The output of our system for the **dev** set is in `/sys_output/sys_output_dev.txt`.
The output of our system for the **test** set is in `/sys_output/sys_output_test.txt`.
