# DRS2AMR


This project accompanies our paper "Bridging Semantic Frameworks: mapping DRS onto AMR", presented at IWCS 2023 ([slides](https://gitlab.inria.fr/semagramme-public-projects/drs2amr/-/blob/main/iwcs2023_slides.pdf)). You can find it [here](https://inria.hal.science/hal-04129563v2).

Our code and details on how to run it are [here](https://gitlab.inria.fr/semagramme-public-projects/drs2amr/-/tree/main/code).

Our data is [here](https://gitlab.inria.fr/semagramme-public-projects/drs2amr/-/tree/main/data).
