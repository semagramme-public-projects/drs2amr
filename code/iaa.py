import os
import re
import subprocess
import pprint
import argparse

pp = pprint.PrettyPrinter(indent=4)


def multiple_sentences_to_multiple_files(load_file, save_folder):
    '''
    Split a file containing multiple annotations in Penman format
    into separate files.
    '''
    with open(load_file, "r") as f:
        lines = f.read()

    lines = re.sub(r'(\n\s*)+\n+', '\n\n', lines)

    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    amrs = lines.split('\n\n')

    for amr in amrs:
        sent_id = amr.split("::id")[-1].split('\n')[0].strip()
        with open(save_folder + sent_id + ".txt", "w") as f:
            f.write(amr)


def get_smatch_score(amr_file1, amr_file2, path_to_smatch):
    '''
    Get Smatch score for a pair of files.
    '''

    subprocess_cmd = f'python3 {path_to_smatch} -f {amr_file1} {amr_file2} --pr'
    try:
        response = subprocess.check_output(subprocess_cmd, shell=True)
    except subprocess.CalledProcessError:
        response = "The filenames provided could not be found or opened. Please check and retry."

    return response


def get_all_scores(folder1, folder2, annotator1, annotator2, path_to_smatch):
    '''
    Get Smatch scores for all pairs of sentences in two folders
    '''
    scores = {}
    for file in os.listdir(folder1):
        sent_id = file.split(".txt")[0]
        score = get_smatch_score(folder1 + file, folder2 + file, path_to_smatch)
        precision = float(score.split(b"Precision: ")[1].split(b"\n")[0])
        recall = float(score.split(b"Recall: ")[1].split(b"\n")[0])
        fscore = float(score.split(b"F-score: ")[1].split(b"\n")[0])
        scores[sent_id] = {"precision": precision,
                           "recall": recall,
                           "f-score": fscore,
                           "annotator1": annotator1,
                           "annotator2": annotator2}
    return scores


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run IAA on annotations')
    parser.add_argument('-s', '--smatch', type=str, required=True, help='path to Smatch')
    args = parser.parse_args()

    path_to_smatch = args.smatch

    # =============== Split files ===================

    to_write_to_single_files = [
        # ("./data/gold/by_group/1_A.txt", "./data/gold/by_group/single_files/1/A/"),
        # ("./data/gold/by_group/1_B.txt", "./data/gold/by_group/single_files/1/B/"),
        # ("./data/gold/by_group/2_A.txt", "./data/gold/by_group/single_files/2/A/"),
        # ("./data/gold/by_group/3_A.txt", "./data/gold/by_group/single_files/3/A/"),
        # ("./data/gold/by_group/3_D.txt", "./data/gold/by_group/single_files/3/D/"),
        # ("./data/gold/by_group/4_B.txt", "./data/gold/by_group/single_files/4/B/"),
        # ("./data/gold/by_group/5_B.txt", "./data/gold/by_group/single_files/5/B/"),
        # ("./data/gold/by_group/5_D.txt", "./data/gold/by_group/single_files/5/D/"),
        # ("./data/gold/by_group/6_D.txt", "./data/gold/by_group/single_files/6/D/"),
        # ("./data/gold/by_group/2_C.txt", "./data/gold/by_group/single_files/2/C/"),
        # ("./data/gold/by_group/4_C.txt", "./data/gold/by_group/single_files/4/C/"),
        # ("./data/gold/by_group/6_C.txt", "./data/gold/by_group/single_files/6/C/")
        # ("./data/gold/gold.txt", "./data/gold/all/single_files/"),
        # ("./data/dev/dev.txt", "./data/dev/single_files/")
    ]

    for load_file, save_folder in to_write_to_single_files:
        multiple_sentences_to_multiple_files(load_file, save_folder)

    folder_prefix = os.getcwd() + "/data/gold/by_group/single_files/"

    anno_pairs = [
        # ("1/A/", "1/B/", "A", "B"),
        # ("3/A/", "3/D/", "A", "D"),
        # ("5/B/", "5/D/", "B", "D"),
        # ("2/A/", "2/C/", "A", "C"),
        ("4/B/", "4/C/", "B", "C"),
        # ("6/C/", "6/D/", "C", "D")
    ]

    scores = {}
    for pair in anno_pairs:
        scores.update(get_all_scores(folder_prefix + pair[0],
                                     folder_prefix + pair[1],
                                     pair[2],
                                     pair[3],
                                     path_to_smatch))

    pp.pprint(scores)

    fscores = sorted([(k, v["f-score"]) for k, v in scores.items()])
    for idx, score in fscores:
        print("| ", idx, " | ", f"{score:.2f}", " |")

