import os
import shutil
import csv
import json
import argparse


def make_folder(folder_name):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)


def copy_part_of_pmb(idx, source_folder, save_folder):
    """
    params: idx (list(tuple)) - PMB IDs of the sentences to be processed
                                List of tuples, where the first element
                                is P and D (part of document number as per
                                PMB's indexing)
    """

    for sent in idx:
        make_folder(save_folder + "p" + sent[0] + "/d" + sent[1])

        shutil.copyfile(source_folder + "p" + sent[0] + "/d" + sent[1] + "/en.drs.sbn",
                        save_folder + "p" + sent[0] + "/d" + sent[1] + "/en.drs.sbn")
        shutil.copyfile(source_folder + "p" + sent[0] + "/d" + sent[1] + "/en.raw",
                        save_folder + "p" + sent[0] + "/d" + sent[1] + "/en.raw")


def join_grew_jsons(source_folder, save_folder, filename):
    all_sents = []
    for file in os.listdir(source_folder):
        if os.path.isfile(source_folder + file):
            with open(source_folder + file) as f:
                data = json.load(f)
                all_sents.extend(data)
    make_folder(save_folder)
    with open(save_folder + filename, "w") as f:
        json.dump(all_sents, f)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Get SBN and raw text for a specific PMB split')
    parser.add_argument('-p', '--pmb_gold', type=str, required=True, help='path to PMB gold en')
    args = parser.parse_args()

    path_to_pmb_gold = args.pmb_gold

    # ========= Copying partition SBNs and raw text =========
    snt_indexes = []

    with open(os.getcwd() + "/data/pmb_split_100/00.csv") as f:
        reader = csv.reader(f, delimiter="\t")
        next(reader)  # skip the header

        for row in reader:
            snt_indexes.append((row[0], row[1]))

    copy_part_of_pmb(snt_indexes,
                     path_to_pmb_gold,
                     os.getcwd() + "/data/original/00/")

    # ================= Joining Grew JSONs ================
    join_grew_jsons(os.getcwd() + "/data/original/00_out/",
                    os.getcwd() + "/data/original/",
                    "00_pmb_grew.json")