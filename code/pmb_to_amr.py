import grew
import json
import pprint
import copy
import subprocess
import configparser
import os
import argparse
from tqdm import tqdm

pp = pprint.PrettyPrinter(indent=4)


def load_data(filepath):
    with open(filepath) as f:
        data = json.load(f)
    return data


def add_node(g, u, a):
    """
    TODO: add docstring
    :param g:
    :param u:
    :param a:
    :return:
    """
    g[u] = (a, [])


def add_edge(g, u, e, v):
    """
    TODO: add docstring
    :param g:
    :param u:
    :param e:
    :param v:
    :return:
    """
    if (e, v) not in g[u][1]:
        g[u][1].append((e, v))


def to_grew(json_graph):
    g = dict()
    for n in json_graph["nodes"]:
        # print(n)
        add_node(g, n, json_graph["nodes"][n])
    for e in json_graph["edges"]:
        add_edge(g, e["src"], e["label"], e["tar"])
    return g


def get_text_format(graph, current_node, visited, text_format, tabs):
    """Takes a graph in GREW format and produces its textual representation
    Parameters: graph - a preprocessed graph in GREW format (needs to have variable names
    and a list of incoming relations for each node)

    NB: for the current implementation things like "polarity -",
        "op1 Mary", "day 29" should be expressed as properties
        (and values) of their parent node

                current_node - the current node for the recursion
                visited - a list of already visited nodes
                text_format - the text format so far
                tabs - the number of tabs to be put at the beginning of each new line
    Output: the visited nodes and text format of the graph so far
    """
    close_after = True
    if current_node not in visited:
        # open a bracket and add the variable name and label of the node
        if graph[current_node][0].get("concept"):
            concept = graph[current_node][0]['concept'].lower()
            if "." in concept:
                # Comment the next line to run without the second post-processing step
                concept = graph[current_node][0]['concept'].lower().split(".")[0]
                concept = concept.replace("_", "-")
                # pass
            text_format += '(' + graph[current_node][0]['var_name'] + ' / ' + concept

        # if the node has properties, e.g. "month", "opN", etc, add them
        if graph[current_node][0].get("value"):
            if ":" in graph[current_node][0]["value"] and "\"" not in graph[current_node][0]["value"]:
                graph[current_node][0]["value"] = "\"" + graph[current_node][0]["value"] + "\""
            text_format = text_format + graph[current_node][0]["value"].replace("\\", "")
            close_after = False

        # if the node has children, get their text format and add that; tabulate as well
        if graph[current_node][1]:
            close_after = True
            for child_node in graph[current_node][1]:
                text_format = text_format + '\n' + tabs * '\t' + ':' + child_node[0] + ' '
                visited, text_format = get_text_format(graph, child_node[1], visited, text_format, tabs + 1)
        # close the bracket for that node and mark the node as visited
        if close_after:
            text_format += ')'
        visited.append(current_node)
    else:
        # if the node has been visited, just add its variable name
        text_format += graph[current_node][0]['var_name']
    return visited, text_format


def grew_to_penman(graph):
    """Takes a graph in GREW format and produces its textual representation
    The graph is preprocessed so that a variable name and a list of incoming
    relations is added to each node. The recursive get_text_format function
    is then called
    Parameters: graph - a graph in GREW format
    Output: the graph in text format
    """
    # TODO: maybe the preprocessing (currently done in grew_to_penman() can happen elsewhere

    # For each node add a list of incoming relations by reversing the outgoing ones

    var_names = []

    # Create var_names for each node
    for node in graph:
        # print(graph[node][0])
        if graph[node][0].get("concept"):
            var_name = graph[node][0]['concept'][0].lower()
            # if first letter is not in the var_names, the first letter becomes the var_name
            if var_name not in var_names:
                graph[node][0]['var_name'] = var_name
                var_names.append(var_name)
            else:
                previous = [x for x in var_names if x.startswith(var_name)]
                graph[node][0]['var_name'] = var_name + str(len(previous) + 1)
                var_names.append(var_name)

    # Make an incoming relations dictionary with an empty list (for now) for each node
    incoming_relations = {node: [] for node in graph}

    # Fill the incoming relations list
    for node in graph:
        for relation in graph[node][1]:
            incoming_relations[relation[1]].append((relation[0], node))

    # Find the starting node (root)
    for node in incoming_relations:
        if not incoming_relations[node]:
            starting_node = node
            break

    # Get the text format of the preprocessed graph with a starting node
    _, text_format = get_text_format(graph, starting_node, [], '', 1)
    # print(text_format)
    # print("-------------------")
    return text_format


def postprocessing(g):
    g_top_idx = len(g)
    new_g = copy.deepcopy(g)
    for node in g:
        new_g[node] = g[node]
        for child in g[node][1]:
            if child[0] == "op1":
                child_node = g[child[1]]
                if child_node[0].get("value"):
                    if " " in child_node[0]["value"]:
                        new_g.pop(child[1])
                        ops = child_node[0]["value"].split()
                        # print(new_g[node][1])
                        new_g[node][1] = []
                        for idx, op in enumerate(ops):
                            g_top_idx += 1
                            add_node(new_g, "o"+str(g_top_idx), {"value": "\""+op.replace("\"","")+"\""})
                            new_g[node][1].append(["op"+str(idx+1), "o"+str(g_top_idx)])
    return new_g


def get_smatch_score(amr_file1, amr_file2, path_to_smatch):

    subprocess_cmd = f'python3 {path_to_smatch} -f {amr_file1} {amr_file2} --pr'
    try:
        response = subprocess.check_output(subprocess_cmd, shell=True)
    except subprocess.CalledProcessError:
        response = "The filenames provided could not be found or opened. Please check and retry."

    return response


def get_best_graph(penman_graphs, gold, path_to_smatch, snt_id, text):
    best_graph = penman_graphs[0]
    best_f1 = 0
    for graph in penman_graphs:
        with open("./data/one_graph_output.txt", "w") as f:
            f.write("# ::id " + snt_id + "\n")
            f.write("# ::text " + text + "\n")
            f.write(graph + "\n\n")
        score = get_smatch_score("./data/one_graph_output.txt", gold, path_to_smatch)
        fscore = float(score.split(b"F-score: ")[1].split(b"\n")[0])
        if fscore >= best_f1:
            best_graph = graph
            best_f1 = fscore
    return best_graph, best_f1


if __name__ == "__main__":

    # ============ READ ARGS =============
    parser = argparse.ArgumentParser(description='Pass config file to run PMB to AMR.')
    parser.add_argument('-c', '--conf', type=str, required=True, help='path to config file')
    args = parser.parse_args()

    # ============ READ CONFIG FILE =================
    config = configparser.ConfigParser()
    config.sections()

    config.read(args.conf)

    data_path = config['all']['path_to_json']
    id_mapping_path = config['all']['id_mapping']
    grs_file = config['all']['grs_file']
    strategy = config['all']['strategy']
    output_path = config['all']['output_path']
    single_files_dir = config['all']['single_files_dir']
    path_to_smatch = config['all']['path_to_smatch']

    # Initialise Grew, load data, initialise variables
    grew.init()
    data = load_data(data_path)
    with open(id_mapping_path, "r") as read_file:
        id_mapping = json.load(read_file)

    output = []
    f1_macro = 0

    # Run Grew and postprocessing on all sentences
    with tqdm(total=len(data)) as pbar:
        for idx, snt in enumerate(data):
            g = to_grew(snt)
            output_graphs = grew.run(grs_file, g, strategy)
            output_graphs = [postprocessing(graph) for graph in output_graphs]

            snt_id = id_mapping[snt["meta"]["sent_id"]]
            text = snt["meta"]["text"]
            penman_graphs = []

            for graph in output_graphs:
                try:
                    penman_graphs.append(grew_to_penman(graph))
                except:
                    penman_graphs.append("(p / placeholder)")

            # Find and keep best output graph
            best_graph, best_f1 = get_best_graph(penman_graphs,
                                                 single_files_dir + snt_id + ".txt",
                                                 path_to_smatch,
                                                 snt_id,
                                                 text)
            f1_macro += best_f1
            output.append((snt_id, text, best_graph))

            pbar.update(1)
    os.remove("./data/one_graph_output.txt")

    # Sort by sentence ID
    output = sorted(output)

    # Compute macro F1
    f1_macro /= len(data)
    print(f'\nMacro Smatch F1-score: {f1_macro}\nRun Smatch on output file for micro and granular')

    # Save graphs to output file
    with open(output_path, "w") as f:
        for snt in output:
            f.write("# ::id " + snt[0] + "\n")
            f.write("# ::text " + snt[1] + "\n")
            f.write(snt[2] + "\n\n")

