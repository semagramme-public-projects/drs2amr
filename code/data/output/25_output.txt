# ::id 25001
# ::text Many a quarrel comes about through a misunderstanding.
(c / come-about
	:Causer (m2 / misunderstanding)
	:ARG1 (q / quarrel
		:quant (m / many)))

# ::id 25002
# ::text She sobbed heavily.
(s / sob
	:manner (h / heavily)
	:ARG0 (s2 / she))

# ::id 25003
# ::text That was incredible, wasn't it?
(i / incredible
	:domain (e / entity))

# ::id 25004
# ::text The rope broke under the strain.
(b / break-01
	:Causer (s / strain)
	:ARG1 (r / rope))

# ::id 25005
# ::text I love pizza.
(l / love-01
	:ARG1 (p / pizza)
	:ARG0 (i / i))

# ::id 25006
# ::text Mr Joel is now on duty.
(o / on
	:ARG1 (d / duty)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Joel")
		:Title (m / mr)))

# ::id 25007
# ::text The wounded were transported with an ambulance.
(t / transport-01
	:instrument (a / ambulance)
	:ARG1 (w / wounded))

# ::id 25008
# ::text He asked me whether I like math.
(a / ask-01
	:ARG1 (l / like-01
		:ARG1 (m / math)
		:ARG0 (i / i))
	:ARG2 i
	:ARG0 (h / he))

# ::id 25009
# ::text Mary wrapped herself in a towel.
(w / wrap-01
	:ARG1 (t / towel)
	:ARG2 (p / person
		:wiki -
		:name (n / name
			:op1 "Mary"))
	:ARG0 p)

# ::id 25010
# ::text He looked like a doctor.
(l / look-like
	:ARG2 (d / doctor)
	:ARG1 (h / he))

# ::id 25011
# ::text A cold bath refreshed him.
(r / refresh-01
	:ARG1 (h / he)
	:ARG0 (b / bath
		:mod (c / cold
			:ARG2 +)))

# ::id 25012
# ::text That cut looks infected.
(l / look-02
	:ARG1 (i / infected
		:domain (c / cut))
	:ARG1 c)

# ::id 25013
# ::text Liisa was sick of the noise.
(s / sick
	:ARG1 (n2 / noise)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Liisa")))

# ::id 25014
# ::text He ran away from home.
(r / run-away
	:source (h / home)
	:ARG1 (h2 / he))

# ::id 25015
# ::text My little sister looks like my mum.
(l / look-like
	:ARG1 (h / have-rel-role-91
		:ARG1 (i / i)
		:ARG2 (l2 / little-sister)
		:ARG0 (p2 / person))
	:ARG2 (h2 / have-rel-role-91
		:ARG1 i
		:ARG2 (m / mum)
		:ARG0 (p / person)))

# ::id 25016
# ::text What city did Duke Ellington live in?
(l / live-01
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Duke"
			:op2 "Ellington"))
	:location (c / city
		:Name (a / amr-unknown)))

# ::id 25017
# ::text The bear ate an apple.
(e / eat-01
	:ARG1 (a / apple)
	:ARG0 (b / bear))

# ::id 25018
# ::text Tom is strong.
(s / strong
	:mod (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 25019
# ::text I like picnics.
(l / like-01
	:ARG1 (p / picnic)
	:ARG0 (i / i))

# ::id 25020
# ::text The red skirt is new.
(n / new
	:mod (s / skirt
		:Colour (r / red)))

# ::id 25021
# ::text She's wearing a nice hat.
(w / wear-01
	:ARG1 (h / hat
		:mod (n / nice))
	:ARG0 (s / she))

# ::id 25022
# ::text I won!
(w / win-01
	:ARG0 (i / i))

# ::id 25023
# ::text She is French.
(b / be-01
	:source (c / country
		:wiki "French"
		:name (n / name
			:op1 "French"))
	:ARG1 (s / she))

# ::id 25024
# ::text I owe her thirty dollars.
(o / owe-01
	:ARG3 (m / monetary-quantity
		:quant 30
		:unit (d / dollar))
	:ARG2 (s / she)
	:ARG0 (i / i))

# ::id 25025
# ::text The novels he wrote are interesting.
(w / write-01
	:ARG0 (h / he)
	:ARG2 (n / novel
		:mod (i / interesting)))

# ::id 25026
# ::text I don't speak Hebrew.
(s / speak-01
	:polarity -
	:ARG3 (h / hebrew)
	:ARG0 (i / i))

# ::id 25027
# ::text What does the abbreviation IRA stand for?
(s / stand-for
	:ARG1 (a2 / abbreviation
		:EQU "IRA")
	:ARG2 (e / entity
		:Name (a / amr-unknown)))

# ::id 25028
# ::text I always get up at 6 o'clock in the morning.
(m / morning)

# ::id 25029
# ::text I can't beat Tom at chess.
(p / possible-01
	:polarity -
	:ARG1 (b / beat-03
		:ARG2 (c / chess)
		:ARG1 (p2 / person
			:wiki -
			:name (n / name
				:op1 "Tom"))
		:ARG0 (i / i)))

# ::id 25030
# ::text Where did you guys meet?
(m / meet-03
	:ARG0 (g / guy
		:SubOf (y / you))
	:location (l / location
		:Name (a / amr-unknown)))

# ::id 25031
# ::text I'm always bored with his boastful talk.
(b2 / bored
	:ARG1 (t / talk
		:mod (b / boastful)
		:Creator (h / he))
	:ARG0 (w / we))

# ::id 25032
# ::text The Normandy landings took place in June 1944.
(t2 / take-place
	:time (t / time
		:MonthOfYear 6
		:YearOfCentury 1944
		:TPR now)
	:ARG1 (l / landing
		:location (s / state
			:wiki "Normandy"
			:name (n / name
				:op1 "Normandy"))))

# ::id 25033
# ::text Tom was surprised the police knew his name.
(s / surprised
	:ARG1 (k / know-01
		:ARG1 (n2 / name
			:Bearer (p2 / person
				:wiki -
				:name (n / name
					:op1 "Tom")))
		:ARG0 (p / police))
	:ARG0 p2)

# ::id 25034
# ::text The oil pipeline is leaking.
(l / leak-01
	:ARG0 (o / oil-pipeline))

# ::id 25035
# ::text The baby is sleeping in the cradle.
(s / sleep-01
	:location (l / location
		:STI (c / cradle))
	:ARG0 (b / baby))

# ::id 25036
# ::text The Mississippi River flows into the Gulf of Mexico.
(f / flow-01
	:destination (g / gulf
		:wiki "Gulf of Mexico"
		:name (n / name
			:op1 "Gulf"
			:op2 "of"
			:op3 "Mexico"))
	:ARG1 (r / river
		:wiki "Mississippi River"
		:name (n2 / name
			:op1 "Mississippi"
			:op2 "River")))

# ::id 25037
# ::text Tom has dyed his hair black.
(d / dye
	:ARG2 (b / black
		:ColourOf (h / hair
			:part-of (p / person
				:wiki -
				:name (n / name
					:op1 "Tom"))))
	:ARG1 h
	:ARG0 p)

# ::id 25038
# ::text He's a very capable business man.
(b / businessman
	:mod (c / capable
		:degree (v / very))
	:domain (h / he))

# ::id 25039
# ::text What group sang the song "Happy Together"?
(s2 / sing-01
	:ARG1 (s / song
		:wiki "Happy Together"
		:name (n / name
			:op1 "Happy"
			:op2 "Together"))
	:ARG0 (g / group
		:Name (a / amr-unknown)))

# ::id 25040
# ::text She roasted the turkey.
(r / roast
	:ARG1 (t / turkey)
	:ARG0 (s / she))

# ::id 25041
# ::text Tom suspected that his father was dyslexic.
(s / suspect-01
	:ARG1 (h / have-rel-role-91
		:ARG2 (f / father)
		:ARG1 (p / person
			:mod (d / dyslexic)
			:EQU (p2 / person
				:wiki -
				:name (n / name
					:op1 "Tom")))
		:ARG0 p2)
	:ARG0 p2)

# ::id 25042
# ::text I am acquainted with Mr Smith.
(a / acquainted
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Smith")
		:Title (m / mr))
	:ARG0 (i / i))

# ::id 25043
# ::text Which suitcases are Tom's?
(b / be-01
	:ARG2 (s / suitcase
		:poss (p / person
			:wiki -
			:name (n / name
				:op1 "Tom")))
	:ARG1 (a / amr-unknown))

# ::id 25044
# ::text She offered me a beer.
(o / offer-01
	:ARG1 (b / beer)
	:beneficiary (i / i)
	:ARG0 (s / she))

# ::id 25045
# ::text I am from Ecuador.
(b / be-01
	:source (c / country
		:wiki "Ecuador"
		:name (n / name
			:op1 "Ecuador"))
	:ARG1 (i / i))

# ::id 25046
# ::text I was talking to him.
(t / talk-01
	:ARG2 (h / he)
	:ARG0 (i / i))

# ::id 25047
# ::text I showered before breakfast.
(s / shower
	:time (t / time
		:TPR now
		:EPR (b / breakfast))
	:ARG0 (i / i))

# ::id 25048
# ::text The park is located in the center of the city.
(l / located
	:location (c2 / center
		:part-of (c / city))
	:ARG1 (p / park))

# ::id 25049
# ::text Tom was very jealous.
(j / jealous
	:degree (v / very)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 25050
# ::text I am boiling water.
(b / boil-01
	:ARG1 (w / water)
	:ARG0 (i / i))

# ::id 25051
# ::text He told me an interesting story.
(i / interesting
	:ARG1 (s / story))

# ::id 25052
# ::text The concert starts at seven. We must not be late.
(o / obligate-01
	:ARG2 (l / late
		:polarity -
		:domain (w / we)))

# ::id 25053
# ::text Tom was run down by a truck.
(r / run-down
	:Causer (t / truck)
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 25054
# ::text Tom retired in 2013.
(r / retire-01
	:time (t / time
		:YearOfCentury 2013
		:TPR now)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 25055
# ::text He knocked at the door.
(k / knock-up-10
	:ARG1 (d / door)
	:ARG0 (h / he))

# ::id 25056
# ::text The dog is mine.
(b / be-01
	:ARG2 (e / entity
		:poss (i / i))
	:ARG1 (d / dog))

# ::id 25057
# ::text He's hiding something.
(h / hide-01
	:ARG1 (e / entity)
	:ARG0 (h2 / he))

# ::id 25058
# ::text Carol is studying Spanish.
(s2 / study-up-02
	:ARG1 (s / spanish)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Carol")))

# ::id 25059
# ::text I prefer to go by subway, rather than by train.
(p / prefer-01
	:ARG1 (g / go-01
		:polarity -
		:instrument (t / train)
		:instrument (s / subway)
		:ARG1 (i / i))
	:ARG0 i)

# ::id 25060
# ::text I didn't hear my alarm clock.
(h / hear-01
	:polarity -
	:ARG1 (a / alarm-clock
		:poss (i / i))
	:ARG0 i)

# ::id 25061
# ::text Everybody that came to the street was surprised.
(c / come-out-09
	:destination (s2 / street)
	:ARG1 (p / person
		:mod (s / surprised)))

# ::id 25062
# ::text Chuck is insolent.
(i / insolent
	:domain (p / person
		:wiki -
		:name (n / name
			:op1 "Chuck")))

# ::id 25063
# ::text Tom cooked me dinner.
(c / cook-01
	:ARG1 (d / dinner)
	:beneficiary (i / i)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 25064
# ::text The mountains in the Himalayas are higher than those in the Andes.
(h / have-degree-91
	:ARG4 (m / mountain
		:location (r / range
			:wiki "Andes"
			:name (n / name
				:op1 "Andes")))
	:ARG2 (m2 / more)
	:ARG3 (h2 / high)
	:ARG1 (m3 / mountain
		:location (r2 / range
			:wiki "Himalayas"
			:name (n2 / name
				:op1 "Himalayas"))))

# ::id 25065
# ::text He isn't running.
(r / run-02
	:polarity -
	:ARG0 (h / he))

# ::id 25066
# ::text A swarm of mosquitoes followed him.
(f / follow-01
	:ARG1 (h / he)
	:ARG0 (m / mosquito
		:quant (s / swarm)))

# ::id 25067
# ::text Whoever loves me, also loves my dog.
(l / love-01
	:ARG1 (d / dog
		:poss (i / i))
	:ARG0 (p / person))

# ::id 25068
# ::text The pirates sailed the seven seas.
(s2 / sail-01
	:path (s / sea
		:quant 7)
	:ARG1 (p / pirate))

# ::id 25069
# ::text Mario bought a microscope.
(b / buy-01
	:ARG1 (m / microscope)
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Mario")))

# ::id 25070
# ::text I was abducted by extraterrestrials.
(a / abduct-01
	:ARG0 (e / extraterrestrial)
	:ARG1 (i / i))

# ::id 25071
# ::text The horse is not white.
(w / white
	:polarity -
	:ColourOf (h / horse))

# ::id 25072
# ::text How many people died?
(d / die-01
	:ARG1 (p / person
		:quant (a / amr-unknown)))

# ::id 25073
# ::text Germany was allied with Italy in World War II.
(a / allied
	:ARG2 (s / state
		:wiki "Italy"
		:name (n2 / name
			:op1 "Italy")
		:time (w / war
			:wiki "World War II"
			:name (n / name
				:op1 "World"
				:op2 "War"
				:op3 "II")))
	:ARG1 (s2 / state
		:wiki "Germany"
		:name (n3 / name
			:op1 "Germany")))

# ::id 25074
# ::text She planted some pansies in the flower bed.
(p2 / plant-01
	:ARG2 (f / flower-bed)
	:ARG1 (p / pansy)
	:ARG0 (s / she))

# ::id 25075
# ::text Illinois borders on Missouri.
(b / border-01
	:ARG2 (s / state
		:wiki "Missouri"
		:name (n / name
			:op1 "Missouri"))
	:ARG1 (s2 / state
		:wiki "Illinois"
		:name (n2 / name
			:op1 "Illinois")))

# ::id 25076
# ::text Some kids had balloons.
(h / have-03
	:ARG1 (b / balloon)
	:ARG0 (k / kid))

# ::id 25077
# ::text I must have my bicycle repaired.
(o / obligate-01
	:ARG2 (r / repair-01
		:ARG1 (b / bicycle
			:poss (w / we))
		:time (t / time
			:TSU now)
		:beneficiary w))

# ::id 25078
# ::text She's dieting.
(d / diet
	:ARG0 (s / she))

# ::id 25079
# ::text I itch everywhere.
(i / itch
	:ARG0 (i2 / i)
	:time (l / location))

# ::id 25080
# ::text The police accused him of murder.
(a / accuse-01
	:ARG2 (m / murder)
	:ARG1 (h / he)
	:ARG0 (p / police))

# ::id 25081
# ::text Are you helping Miss Hansson?
(h / help-01
	:ARG2 (p / person
		:wiki -
		:name (n / name
			:op1 "Hansson")
		:Title (m / miss))
	:ARG0 (y / you))

# ::id 25082
# ::text http://www.ispaworld.org/canada/rules1.html
(e / event
	:Participant (u / url
		:wiki "http://www.ispaworld.org/canada/rules1.html"
		:name (n / name
			:op1 "http://www.ispaworld.org/canada/rules1.html")))

# ::id 25083
# ::text The house caved in.
(c / cave-in
	:ARG1 (h / house))

# ::id 25084
# ::text 'What is that?' asked Tony.
(a / ask-01
	:ARG1 (b / be-01
		:ARG1 (e / entity)
		:ARG2 (w / we))
	:ARG0 (p / person
		:wiki -
		:name (n / name
			:op1 "Tony")))

# ::id 25085
# ::text The line is engaged.
(e / engaged
	:ARG1 (l / line))

# ::id 25086
# ::text This bridge was built two years ago.
(b / build-01
	:time (t2 / time
		:TPR now
		:TAB (t / temporal-quantity
			:quant 2
			:TAB now
			:unit (y / year)))
	:ARG1 (b2 / bridge))

# ::id 25087
# ::text Tom kicked Mary.
(k / kick-01
	:ARG1 (p / person
		:wiki -
		:name (n / name
			:op1 "Mary"))
	:ARG0 (p2 / person
		:wiki -
		:name (n2 / name
			:op1 "Tom")))

# ::id 25088
# ::text I am learning Chinese.
(l / learn-01
	:ARG1 (c / chinese)
	:ARG0 (i / i))

# ::id 25089
# ::text Vladislav Listyev was murdered in Moscow.
(m / murder-01
	:location (c / city
		:wiki "Moscow"
		:name (n / name
			:op1 "Moscow"))
	:ARG1 (p / person
		:wiki -
		:name (n2 / name
			:op1 "Vladislav"
			:op2 "Listyev")))

# ::id 25090
# ::text Tom is pissed.
(p / pissed
	:mod (p2 / person
		:wiki -
		:name (n / name
			:op1 "Tom")))

# ::id 25091
# ::text This novel bores me.
(b / bore
	:ARG0 (w / we)
	:ARG1 (n / novel))

# ::id 25092
# ::text He's cleaning out his closet.
(c2 / clean-out
	:source (c / closet
		:poss (h / he))
	:ARG0 h)

# ::id 25093
# ::text A number of tourists were injured in the accident.
(i / injure-01
	:Causer (a / accident)
	:ARG1 (t / tourist
		:quant (q / quantity
			:EQU +)))

# ::id 25094
# ::text He crawled out of bed.
(c / crawl-01
	:source (b / bed)
	:ARG0 (h / he))

# ::id 25095
# ::text How many carjackings took place in Los Angeles in 1991?
(t2 / take-place
	:location (c / city
		:wiki "Los Angeles"
		:name (n / name
			:op1 "Los"
			:op2 "Angeles"))
	:time (t / time
		:YearOfCentury 1991
		:TPR now)
	:ARG1 (c2 / carjacking
		:quant (a / amr-unknown)))

# ::id 25096
# ::text I shouldn't have yelled at Tom.
(r / recommend-01
	:ARG1 (y / yell-01
		:polarity -
		:ARG2 (p / person
			:wiki -
			:name (n / name
				:op1 "Tom"))
		:ARG0 (i / i)))

# ::id 25097
# ::text A dollar is equal to a hundred cents.
(e / equal
	:ARG2 (m / monetary-quantity
		:quant 100
		:unit (c / cent))
	:ARG1 (m2 / monetary-quantity
		:quant 1
		:unit (d / dollar)))

# ::id 25098
# ::text I gave the books to this student.
(g / give-01
	:ARG2 (s / student)
	:ARG1 (b / book)
	:ARG0 (i / i))

# ::id 25099
# ::text Tom put a bunch of letters on Mary's desk.
(p2 / put-01
	:ARG2 (d / desk
		:poss (p / person
			:wiki -
			:name (n / name
				:op1 "Mary")))
	:ARG1 (l / letter
		:quant (b / bunch))
	:ARG0 (p3 / person
		:wiki -
		:name (n2 / name
			:op1 "Tom")))

# ::id 25100
# ::text I'll return at 6:30.
(r / return-01
	:time (t / time
		:ClockTime "18:30"
		:TSU now)
	:ARG1 (i / i))

# ::id 25101
# ::text I don't speak Irish.
(s / speak-01
	:polarity -
	:ARG3 (i / irish)
	:ARG0 (i2 / i))

# ::id 25102
# ::text Did you hurt yourself?
(h / hurt-02
	:ARG1 (y / you))

