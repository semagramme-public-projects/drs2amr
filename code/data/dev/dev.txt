# ::id 00001
# ::text I began playing golf years ago.
(b / begin-01
    :ARG0 (i / i)
    :ARG1 (p / play-01
        :ARG0 i
        :ARG1 (g / golf))
    :time (b2 / before
        :op1 (n / now)
        :quant (t / temporal-quantity
            :unit (y / year))))

# ::id 00002
# ::text I cooked him dinner.
(c / cook-01
    :ARG0 (i / i)
    :ARG1 (d / dinner)
    :beneficiary (h / he))

# ::id 00003
# ::text Aren't those your parents?
(h / have-rel-role-91
    :ARG0 (t / that)
    :ARG1 (y / you)
    :ARG2 (p / parent)
    :ARG1-of (r / request-confirmation))

# ::id 00004
# ::text I bit my tongue.
(b / bite-01
    :ARG0 (i / i)
    :ARG1 (t / tongue
        :part-of i))

# ::id 00005
# ::text The matches cost ten pennies.
(c / cost-01
    :ARG1 (m / match)
    :ARG2 (m2 / monetary-quantity
        :quant 10
        :unit (p / penny)))

# ::id 00006
# ::text I kind of liked Tom.
(l / like-01
    :ARG0 (i / i)
    :ARG1 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :degree (k / kind-of))

# ::id 00007
# ::text I think my girlfriend is kind of cute.
(t / think-01
    :ARG0 (i / i)
    :ARG1 (c / cute-01
        :ARG1 (p / person
            :ARG0-of (h / have-rel-role-91
                :ARG1 i
                :ARG2 (g / girlfriend)))
        :degree (k / kind-of)))

# ::id 00008
# ::text He is Japanese.
(h / he
    :mod (c / country
        :name (n / name :op1 "Japanese")
        :wiki "Japan"))

# ::id 00009
# ::text Do you speak Hebrew?
(s / speak-01
    :ARG0 (y / you)
    :ARG3 (l / language
        :name (n / name :op1 "Hebrew")
        :wiki "Hebrew_language")
    :polarity (a / amr-unknown))

# ::id 00010
# ::text We must destroy the evidence.
(o / obligate-01
    :ARG2 (d / destroy-01
        :ARG0 (w / we)
        :ARG1 (e / evidence)))

# ::id 00011
# ::text I also lost my cell phone!
(l / lose-02
    :ARG0 (i / i)
    :ARG1 (c / cell-phone
        :poss i)
    :mod (a / also))

# ::id 00012
# ::text She has a rare given name.
(h / have-03
    :ARG0 (s / she)
    :ARG1 (g / given-name
        :mod (r / rare)))

# ::id 00013
# ::text Tom punched Mary in the stomach.
(p / punch-01
    :ARG0 (p2 / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (p3 / person
        :name (n2 / name :op1 "Mary")
        :wiki -)
    :location (s / stomach
        :part-of p3))

# ::id 00014
# ::text He was attacked by a shark.
(a / attack-01
    :ARG0 (s / shark)
    :ARG1 (h / he))

# ::id 00015
# ::text I apologize. It was my mistake.
(m / multi-sentence
    :snt1 (a / apologize-01
        :ARG0 (i / i))
    :snt2 (m2 / mistake-02
        :ARG1 (i2 / i)))

# ::id 00016
# ::text Tom chickened out.
(c / chicken-out-01
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -))

# ::id 00017
# ::text There are always a lot of vehicles on this road.
(b / be-located-at-91
    :ARG1 (v / vehicle
        :quant (l / lot))
    :ARG2 (r / road
        :mod (t / this))
    :frequency (a / always))

# ::id 00018
# ::text Tom told me that he'd lost his watch.
(t / tell-01
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (l / lose-02
        :ARG0 p
        :ARG1 (w / watch))
    :ARG2 (i / i))

# ::id 00019
# ::text The appendix is about 10 cm long.
(l / long-03
    :ARG1 (a / appendix)
    :ARG2 (a2 / about
        :op1 (d / distance-quantity
            :quant 10
            :unit (c / centimeter))))

# ::id 00020
# ::text He drank beer.
(d / drink-01
    :ARG0 (h / he)
    :ARG1 (b / beer))

# ::id 00021
# ::text Tom seems worried.
(s / seem-01
    :ARG1 (w / worry-01
        :ARG1 (p / person
            :name (n / name :op1 "Tom")
            :wiki -)))

# ::id 00022
# ::text Leo Tolstoy was a vegetarian.
(v / vegetarian-01
    :ARG1 (p / person
        :name (n / name
            :op1 "Leo"
            :op2 "Tolstoy")
        :wiki "Leo_Tolstoy"))

# ::id 00023
# ::text "Who sang ""Tennessee Waltz""?"
(s / sing-01
    :ARG0 (a / amr-unknown)
    :ARG1 (w / work-of-art
        :name (n / name
            :op1 "Tennessee"
            :op2 "Waltz")
        :wiki "Tennessee_Waltz"))

# ::id 00024
# ::text They are holding it.
(h / hold-01
    :ARG0 (t / they)
    :ARG1 (i / it))

# ::id 00025
# ::text Her favourite band is Warpaint.
(l / like-01
    :ARG0 (s / she)
    :ARG1 (b / band
        :name (n / name :op1 "Warpaint")
        :wiki "Warpaint_(band)")
    :degree (m / most))

# ::id 00026
# ::text Dick played piano and Lucy sang.
(a / and
    :op1 (p / play-11
        :ARG0 (p2 / person
            :name (n / name :op1 "Dick")
            :wiki -)
        :ARG2 (p3 / piano))
    :op2 (s / sing-01
        :ARG0 (p4 / person
            :name (n2 / name :op1 "Lucy")
            :wiki -)))

# ::id 00027
# ::text He's dark and handsome.
(a / he
    :mod (d / dark-01)
    :mod (h2 / handsome))

# ::id 00028
# ::text This television set is heavy.
(h / heavy-01
    :ARG1 (t / television-set
        :mod (t2 / this)))

# ::id 00029
# ::text I took two aspirin for my headache.
(t / take-01
    :ARG0 (i / i)
    :ARG1 (a / aspirin
        :quant 2)
    :purpose (h / headache
        :poss i))

# ::id 00030
# ::text Tom is paying a fine.
(p / pay-01
    :ARG0 (p2 / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG3 (t / thing
        :ARG1-of (f / fine-01)))

# ::id 00031
# ::text A flute is being played by a girl
(p / play-11
    :ARG0 (g / girl)
    :ARG1 (f / flute))

# ::id 00032
# ::text European cars sell in Russia.
(s / sell-01
    :ARG1 (c / car
        :mod (c2 / continent
            :name (n / name :op1 "European")
            :wiki "Europe"))
    :location (c3 / country
        :name (n2 / name :op1 "Russia")
        :wiki "Russia"))

# ::id 00033
# ::text Tom is manipulative.
(m / manipulative
    :domain (p / person
        :name (n / name :op1 "Tom")
        :wiki -))

# ::id 00034
# ::text Maybe Tom killed himself.
(p / possible-01
    :ARG1 (k / kill-01
        :ARG0 (p2 / person
            :name (n / name :op1 "Tom")
            :wiki -)
        :ARG1 p2))

# ::id 00035
# ::text Whose is this camera?
(c / camera
    :poss (a / amr-unknown)
    :mod (t / this))

# ::id 00036
# ::text The game excited lots of people.
(e / excite-01
    :ARG0 (g / game)
    :ARG1 (p / person
        :quant (l / lot)))

# ::id 00037
# ::text He cooked me dinner.
(c / cook-01
    :ARG0 (h / he)
    :product (d / dinner)
    :beneficiary (i / i))

# ::id 00038
# ::text Tom is extremely unfriendly.
(f / friendly-01
    :ARG1 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :polarity -
    :degree (e / extreme))

# ::id 00039
# ::text There were many rotten apples in the basket.
(b / be-located-at-91
    :ARG1 (a / apple
        :quant (m / many)
        :mod (r / rotten))
    :ARG2 (b2 / basket))

# ::id 00040
# ::text The girl lacked musical ability.
(l / lack-01
    :ARG0 (g / girl)
    :ARG1 (a / ability
        :mod (m / music)))

# ::id 00041
# ::text This car is bigger than that one.
(h / have-degree-91
    :ARG1 (c / car
        :mod (t / this))
    :ARG2 (m / more)
    :ARG3 (b / big-01)
    :ARG4 (c2 / car
        :mod (t2 / that)))

# ::id 00042
# ::text I am convinced that he is innocent.
(c / convince-01
    :ARG1 (i / i)
    :ARG2 (i2 / innocent-01
        :ARG1 (h / he)))

# ::id 00043
# ::text Can you turn on the air conditioning?
(t / turn-on-13
    :ARG0 (y / you)
    :ARG1 (a / air-conditioning)
    :mode (i / imperative)
    :polite +)

# ::id 00044
# ::text He is named Jeff by everyone.
(n / name-01
    :ARG0 (e / everyone)
    :ARG1 (h / he)
    :ARG2 (n2 / name
        :op1 "Jeff"))

# ::id 00045
# ::text Tom took his finger off the trigger.
(t / take-off-07
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (f / finger
        :part-of p)
    :location (t2 / trigger))

# ::id 00046
# ::text Name a poem by Eugenio Montale.
(n / name-01
    :ARG0 (y / you)
    :ARG1 (p / poem
        :ARG1-of (a / author-01
            :ARG0 (p2 / person
                :name (n2 / name :op1 "Eugenio Montale")
                :wiki "Eugenio_Montale")))
    :mode (i / imperative))

# ::id 00047
# ::text He tried to seduce me.
(t / try-01
    :ARG0 (h / he)
    :ARG1 (s / seduce-01
        :ARG0 h
        :ARG1 (i / i)))

# ::id 00048
# ::text His house is very far from the station.
(f / far-01
    :ARG1 (h / house
        :poss (h2 / he))
    :ARG2 (s / station)
    :degree (v / very))

# ::id 00049
# ::text I abhor spiders.
(a / abhor-01
    :ARG0 (i / i)
    :ARG1 (s / spider))

# ::id 00050
# ::text A cow is eating hay
(e / eat-01
    :ARG0 (c / cow)
    :ARG1 (h / hay))

# ::id 00051
# ::text She accompanied me on the piano.
(a / accompany-01
    :ARG0 (s / she)
    :ARG1 (i / i)
    :instrument (p / piano))

# ::id 00052
# ::text The trees are green.
(g / green-02
    :ARG1 (t / tree))

# ::id 00053
# ::text How did Harry Houdini die?
(d / die-01
    :ARG1 (p / person
        :name (n / name :op1 "Harry" :op2 "Houdini")
        :wiki "Harry_Houdini")
    :manner (a / amr-unknown))

# ::id 00054
# ::text A mile is equal to about 1600 meters.
(e / equal-01
    :ARG1 (d / distance-quantity
        :quant 1
        :unit (m / mile))
    :ARG2 (a / about
        :op1 (d2 / distance-quantity
            :quant 1600
            :unit (m2 / meter))))

# ::id 00055
# ::text Tom unbuckled his seatbelt.
(u / unbuckle-01
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (s / seatbelt
        :poss p))

# ::id 00056
# ::text He gave me a half dozen linen handkerchiefs.
(g / give-01
    :ARG0 (h / he)
    :ARG1 (h2 / handkerchief
        :mod (l / linen)
        :quant 6)
    :ARG2 (i / i))

# ::id 00057
# ::text The old lady smiled at her granddaughter.
(s / smile-01
    :ARG0 (l / lady
        :mod (o / old))
    :ARG2 (p / person
        :ARG0-of (h / have-rel-rol-91
            :ARG1 l
            :ARG2 (g / granddaughter))))

# ::id 00058
# ::text This squid is five quids.
(c / cost-01
    :ARG1 (s / squid
        :mod (t / this))
    :ARG2 (m / monetary-quantity
        :quant 5
        :unit (q / quid)))

# ::id 00059
# ::text """I was a boy,"" he said impatiently."
(s / say-01
    :ARG0 (h / he)
    :ARG1 (b / boy
        :domain h)
    :manner (p / patient-01
        :polarity -))

# ::id 00060
# ::text How many people survived the Auschwitz concentration camp?
(s / survive-01
    :ARG0 (p / person
        :quant (a / amr-unknown))
    :ARG1 (c / concentration-camp
        :name (n / name :op1 "Auschwitz" :op2 "concentration" :op3 "camp")
        :wiki "Auschwitz_concentration_camp"))

# ::id 00061
# ::text The boys set up a hockey rink on the frozen pond.
(s / set-up-03
    :ARG0 (b / boy)
    :ARG1 (h / hockey-rink)
    :location (p / pond
        :mod (f / frozen)))

# ::id 00062
# ::text I'm not outgoing.
(o / outgoing
    :domain (i / i)
    :polarity -)

# ::id 00063
# ::text Tom waved at Mary.
(w / wave-01
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (p2 / person
        :name (n2 / name :op1 "Mary")
        :wiki -))

# ::id 00064
# ::text I grabbed the cat by the neck.
(g / grab-01
    :ARG0 (i / i)
    :ARG1 (c / cat)
    :location (n / neck
        :part-of c))

# ::id 00065
# ::text A parrot can mimic a person's voice.
(p / possible-01
    :ARG1 (m / mimic-01
        :ARG0 (p2 / parrot)
        :ARG1 (v / voice
            :mod (p3 / person))))

# ::id 00066
# ::text The pen has run out of ink.
(r / run-out-05
    :ARG1 (i / ink)
    :ARG2 (p / pen))

# ::id 00067
# ::text I'm at the airport.
(b / be-located-at-91
    :ARG1 (i / i)
    :ARG2 (a / airport))

# ::id 00068
# ::text Tom is trapped.
(t / trap-01
    :ARG1 (p / person
        :name (n / name :op1 "Tom")
        :wiki -))

# ::id 00069
# ::text It snowed for ten consecutive days.
(s / snow-01
    :duration (t / temporal-quantity
        :quant 10
        :unit (d / day)))

# ::id 00070
# ::text Tom is an excellent tennis player.
(p / play-01
    :ARG0 (p2 / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (t / tennis)
    :degree (e / excellent))

# ::id 00071
# ::text I'm an alcoholic. I'm a drug addict. I'm homosexual. I'm a genius.
(m / multi-sentence
    :snt1 (a / alcoholic
        :domain (i / i))
    :snt2 (a2 / addict-01
        :ARG1 (i2 / i)
        :ARG2 (d / drug))
    :snt3 (h / homosexual
        :domain (i3 / i))
    :snt4 (g / genius
        :domain (i4 / i)))

# ::id 00072
# ::text I can't find the newspaper.
(p / possible-01
    :ARG1 (f / find-01
        :ARG0 (i / i)
        :ARG1 (n / newspaper))
    :polarity -)

# ::id 00073
# ::text He weighs 70 kilos.
(w / weigh-01
    :ARG1 (h / he)
    :ARG3 (m / mass-quantity
        :quant 70
        :unit (k / kilogram)))

# ::id 00074
# ::text Prostitution is legal in Germany.
(l / legal-02
    :ARG1 (p / prostitution)
    :location (c / country
        :name (n / name :op1 "Germany")
        :wiki "Germany"))

# ::id 00075
# ::text Everyone said that I was wrong.
(s / say-01
    :ARG0 (e / everyone)
    :ARG1 (w / wrong-04
        :ARG1 (i / i)))

# ::id 00076
# ::text What scared you?
(s / scare-01
    :ARG0 (a / amr-unknown)
    :ARG1 (y / you))

# ::id 00077
# ::text His slacks are all wrinkled.
(w / wrinkle-01
    :ARG1 (s / slacks
        :poss (h / he)))

# ::id 00078
# ::text Mary combed her fingers through her hair.
(c / comb-02
    :ARG0 (p / person
        :name (n / name :op1 "Mary")
        :wiki -)
    :ARG1 (h / hair
        :part-of p)
    :instrument (f / finger
        :part-of p))

# ::id 00079
# ::text I knew it was unhealthy.
(k / know-01
    :ARG0 (i / i)
    :ARG1 (h / healthy-01
        :ARG1 (i2 / it)
        :polarity -))

# ::id 00080
# ::text Harnold Lamb authored many biographies.
(a / author-01
    :ARG0 (p / person
        :name (n / name :op1 "Harnold" :op2 "Lamb")
        :wiki "Harold_Lamb")
    :ARG1 (b / biography
        :quant (m / many)))

# ::id 00081
# ::text She bit him.
(b / bite-01
    :ARG0 (s / she)
    :ARG1 (h / he))

# ::id 00082
# ::text The clerk labeled the baggage.
(l / label-01
    :ARG0 (c / clerk)
    :ARG1 (b / baggage))

# ::id 00083
# ::text I put a few blueberries in your tea.
(p / put-01
    :ARG0 (i / i)
    :ARG1 (b / blueberry
        :quant (f / few))
    :ARG2 (t / tea
        :poss (y / you)))

# ::id 00084
# ::text This expedition will be expensive.
(e / expensive-02
    :ARG1 (e2 / expedition
        :mod (t / this)))

# ::id 00085
# ::text Every wall is a door.
(d / door
    :domain (w / wall
        :mod (e / every)))

# ::id 00086
# ::text I am a tennis player.
(t/ tennis-player
    :domain (i / i))

# ::id 00087
# ::text None of the cars is mine.
(c / car
    :quant (n / none)
    :poss (i / i))

# ::id 00088
# ::text There's a hole in this sock.
(b / be-located-at-91
    :ARG1 (h / hole)
    :ARG2 (s / sock
        :mod (t / this)))

# ::id 00089
# ::text Yedinstvo.
(p / political-party
    :name (n / name :op1 "Yedinstvo")
    :wiki "Yedinstvo")

# ::id 00090
# ::text Tom is skinny.
(s / skinny
    :domain (p / person
        :name (n / name :op1 "Tom")
        :wiki -))

# ::id 00091
# ::text My car is being repaired.
(r / repair-01
    :ARG1 (c / car
        :poss (i / i)))

# ::id 00092
# ::text Tom now lives in Boston.
(l / live-01
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :location (c / city
        :name (n2 / name :op1 "Boston")
        :wiki "Boston")
    :time (n3 / now))

# ::id 00093
# ::text There is a pen on the desk.
(b / be-located-at-91
    :ARG1 (p / pen)
    :ARG2 (d / desk))

# ::id 00094
# ::text This afternoon, Becker lost to Austrian Gilbert Schaller (2:6, 2:6).
(l / lose-03
    :ARG0 (p / person
        :name (n / name :op1 "Becker")
        :wiki "Boris_Becker")
    :ARG2 (p2 / person
        :name (n2 / name :op1 "Gilbert" :op2 "Schaller")
        :wiki "Gilbert_Schaller"
        :ARG1-of (b / be-from-91
            :ARG2 (c / country
                :name (n3 / name :op1 "Austrian")
                :wiki "Austria")))
    :time (d / date-entity
        :dayperiod (a / afternoon
            :mod (t / this)))
    :score-entity "(2:6, 2:6)")

# ::id 00095
# ::text Emily is afraid of spiders.
(f / fear-01
    :ARG0 (p / person
        :name (n / name :op1 "Emily")
        :wiki -)
    :ARG1 (s / spider))
