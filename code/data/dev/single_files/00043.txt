# ::id 00043
# ::text Can you turn on the air conditioning?
(t / turn-on-13
    :ARG0 (y / you)
    :ARG1 (a / air-conditioning)
    :mode (i / imperative)
    :polite +)