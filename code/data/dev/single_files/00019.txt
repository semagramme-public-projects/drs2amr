# ::id 00019
# ::text The appendix is about 10 cm long.
(l / long-03
    :ARG1 (a / appendix)
    :ARG2 (a2 / about
        :op1 (d / distance-quantity
            :quant 10
            :unit (c / centimeter))))