# ::id 00005
# ::text The matches cost ten pennies.
(c / cost-01
    :ARG1 (m / match)
    :ARG2 (m2 / monetary-quantity
        :quant 10
        :unit (p / penny)))