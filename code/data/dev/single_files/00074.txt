# ::id 00074
# ::text Prostitution is legal in Germany.
(l / legal-02
    :ARG1 (p / prostitution)
    :location (c / country
        :name (n / name :op1 "Germany")
        :wiki "Germany"))