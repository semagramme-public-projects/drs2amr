# ::id 00023
# ::text "Who sang ""Tennessee Waltz""?"
(s / sing-01
    :ARG0 (a / amr-unknown)
    :ARG1 (w / work-of-art
        :name (n / name
            :op1 "Tennessee"
            :op2 "Waltz")
        :wiki "Tennessee_Waltz"))