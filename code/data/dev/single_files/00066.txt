# ::id 00066
# ::text The pen has run out of ink.
(r / run-out-05
    :ARG1 (i / ink)
    :ARG2 (p / pen))