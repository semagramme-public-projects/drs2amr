# ::id 00001
# ::text I began playing golf years ago.
(b / begin-01
    :ARG0 (i / i)
    :ARG1 (p / play-01
        :ARG0 i
        :ARG1 (g / golf))
    :time (b2 / before
        :op1 (n / now)
        :quant (t / temporal-quantity
            :unit (y / year))))