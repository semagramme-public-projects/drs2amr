# ::id 00041
# ::text This car is bigger than that one.
(h / have-degree-91
    :ARG1 (c / car
        :mod (t / this))
    :ARG2 (m / more)
    :ARG3 (b / big-01)
    :ARG4 (c2 / car
        :mod (t2 / that)))