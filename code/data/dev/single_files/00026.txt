# ::id 00026
# ::text Dick played piano and Lucy sang.
(a / and
    :op1 (p / play-11
        :ARG0 (p2 / person
            :name (n / name :op1 "Dick")
            :wiki -)
        :ARG2 (p3 / piano))
    :op2 (s / sing-01
        :ARG0 (p4 / person
            :name (n2 / name :op1 "Lucy")
            :wiki -)))