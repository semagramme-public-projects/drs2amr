# ::id 00010
# ::text We must destroy the evidence.
(o / obligate-01
    :ARG2 (d / destroy-01
        :ARG0 (w / we)
        :ARG1 (e / evidence)))