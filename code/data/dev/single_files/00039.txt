# ::id 00039
# ::text There were many rotten apples in the basket.
(b / be-located-at-91
    :ARG1 (a / apple
        :quant (m / many)
        :mod (r / rotten))
    :ARG2 (b2 / basket))