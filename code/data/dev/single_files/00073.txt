# ::id 00073
# ::text He weighs 70 kilos.
(w / weigh-01
    :ARG1 (h / he)
    :ARG3 (m / mass-quantity
        :quant 70
        :unit (k / kilogram)))