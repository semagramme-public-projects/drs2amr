# ::id 00078
# ::text Mary combed her fingers through her hair.
(c / comb-02
    :ARG0 (p / person
        :name (n / name :op1 "Mary")
        :wiki -)
    :ARG1 (h / hair
        :part-of p)
    :instrument (f / finger
        :part-of p))