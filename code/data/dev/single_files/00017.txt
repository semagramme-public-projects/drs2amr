# ::id 00017
# ::text There are always a lot of vehicles on this road.
(b / be-located-at-91
    :ARG1 (v / vehicle
        :quant (l / lot))
    :ARG2 (r / road
        :mod (t / this))
    :frequency (a / always))