# ::id 00045
# ::text Tom took his finger off the trigger.
(t / take-off-07
    :ARG0 (p / person
        :name (n / name :op1 "Tom")
        :wiki -)
    :ARG1 (f / finger
        :part-of p)
    :location (t2 / trigger))