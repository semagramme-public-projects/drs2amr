# ::id 00094
# ::text This afternoon, Becker lost to Austrian Gilbert Schaller (2:6, 2:6).
(l / lose-03
    :ARG0 (p / person
        :name (n / name :op1 "Becker")
        :wiki "Boris_Becker")
    :ARG2 (p2 / person
        :name (n2 / name :op1 "Gilbert" :op2 "Schaller")
        :wiki "Gilbert_Schaller"
        :ARG1-of (b / be-from-91
            :ARG2 (c / country
                :name (n3 / name :op1 "Austrian")
                :wiki "Austria")))
    :time (d / date-entity
        :dayperiod (a / afternoon
            :mod (t / this)))
    :score-entity "(2:6, 2:6)")