# ::id 25075
# ::text Illinois borders on Missouri.
(b / border-01
    :ARG1 (s / state
        :name (n / name :op1 "Illinois")
        :wiki "Illinois")
    :ARG1 (s2 / state
        :name (n2 / name :op1 "Missouri")
        :wiki "Missouri"))