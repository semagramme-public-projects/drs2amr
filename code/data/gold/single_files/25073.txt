# ::id 25073
# ::text Germany was allied with Italy in World War II.
(a / ally-01
    :ARG1 (c / country
        :name (n / name :op1 "Germany")
        :wiki "Germany")
    :ARG2 (c2 / country
        :name (n2 / name :op1 "Italy")
        :wiki "Italy")
    :time (w / war
        :name (n3 / name :op1 "World" :op2 "War" :op3 "II")
        :wiki "World_War_II"))