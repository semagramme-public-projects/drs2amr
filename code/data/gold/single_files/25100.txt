# ::id 25100
# ::text I'll return at 6:30.
(r / return-01
    :ARG1 (i / i)
    :time (d / date-entity
        :time "06:30")
)