# ::id 25006
# ::text Mr Joel is now on duty.
(o / on-duty
  :domain (p / person
    :wiki -
    :name (n / name
      :op1 "Mr"
      :op2 "Joel"))
  :time (n8 / now))