# ::id 25037
# ::text Tom has dyed his hair black.
(d / dye-01
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG1 (h / hair
    :poss p)
  :ARG2 (b / black-04))