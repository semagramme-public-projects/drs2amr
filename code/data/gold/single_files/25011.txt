# ::id 25011
# ::text A cold bath refreshed him.
(r / refresh-01
  :ARG0 (b / bath
    :ARG1-of (c / cold-01))
  :ARG1 (h / he))