# ::id 25001
# ::text Many a quarrel comes about through a misunderstanding.
(c / come-about-06
  :ARG1 (q / quarrel-01
    :quant (m / many))
  :ARG2 (t / thing
    :ARG1-of (m2 / misunderstand-01)))