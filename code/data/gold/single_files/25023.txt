# ::id 25023
# ::text She is French.
(s / she
  :mod (c / country
    :wiki "France"
    :name (n / name
      :op1 "France")))