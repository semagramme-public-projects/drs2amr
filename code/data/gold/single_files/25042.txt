# ::id 25042
# ::text I am acquainted with Mr Smith.
(a / acquaint-01
  :ARG0 (i / i)
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Mr."
      :op2 "Smith")))