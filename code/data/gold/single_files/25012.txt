# ::id 25012
# ::text That cut looks infected.
(l / look-02
  :ARG0 (t / thing
    :mod (t2 / that)
    :ARG1-of (c / cut-01))
  :ARG1 (t3 / thing
    :ARG1-of (i / infect-01)))