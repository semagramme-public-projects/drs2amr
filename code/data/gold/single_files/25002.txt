# ::id 25002
# ::text She sobbed heavily.
# Note heavy-01 not in https://amr.isi.edu/doc/propbank-amr-frames-arg-descr.txt
(s / sob-01
  :ARG0 (s2 / she)
  :ARG1-of (h / heavy-01))