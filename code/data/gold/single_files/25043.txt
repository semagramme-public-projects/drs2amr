# ::id 25043
# ::text Which suitcases are Tom's?
# Note inspired by lpp_1943.122
(s / suitcase
      :poss (p / person
        :wiki -
        :name (n / name
          :op1 "Tom"))
      :domain (a / amr-unknown))