#::id 25095
#::text How many carjackings took place in Los Angeles in 1991?
(n / number
        :quant-of (c / carjacking)
	:location (c2 / city
          :wiki "Los_Angeles"
          :name (n2 / name
               :op1 "Los"
               :op2 "Angeles")
	)
	:time (d / date-entity
          :year 1991
	)
	:value amp-unknown
)