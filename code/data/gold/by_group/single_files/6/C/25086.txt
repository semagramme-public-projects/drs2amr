#::id 25086
#::text This bridge was built two years ago.
(bd / build-01
        :ARG1 ( bg / bridge)
	:time (bf / before
		:op1 (n / now)
		:quant (t / temporal-quantity
		:unit (y / year)
		:quant 2)
	)
)