# ::id 25098
# ::text I gave the books to this student.
(g / give-01
    :ARG0 (i / i)
    :ARG1 (b / book)
    :ARG2 (s / student
        :mod (t / this))
)