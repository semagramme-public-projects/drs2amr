# ::id 25101
# ::text I don't speak Irish.
(s / speak-01
    :ARG0 (i / i)
    :ARG3 (l / language
        :wiki "Irish_language"
        :name (n / name
            :op1 "Irish"))
    :polarity -)