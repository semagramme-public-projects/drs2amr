# ::id 25089
# ::text Vladislav Listyev was murdered in Moscow.
(m / murder-01
    :ARG1 (p / person
        :name (n / name
            :op1 "Vladislav"
            :op2 "Listyev")
        :wiki "Vladislav_Listyev")
    :location (c / city
        :name (n2 / name :op1 "Moscow")
        :wiki "Moscow"
    )
)