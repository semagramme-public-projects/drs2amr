# ::id 25058
# ::text Carol is studying Spanish.
(s /study-01
    :ARG0 (p / person :name (n1 /name :op1 "Carol"))
    :ARG1 (l / language :name (n /name :op1 "Spanish")))