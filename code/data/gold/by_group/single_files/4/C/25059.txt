#::id 25059
#::text I prefer to go by subway, rather than by train.
(p / prefer-01
	:ARG0 (i / i)
	:ARG1 (g1 / go-01
		:instrument (s / subway
		)
	)
	:ARG2 (g2 / go-01
		:instrument (t / train
		)
	)
)