#::id 25063
#::text Tom cooked me dinner.
(c / cook-01
	:ARG0 (p /person
		:name (n / name :op1 "Tom")
	)
        :ARG1 (d / dinner)
	:destination (i / i)
)