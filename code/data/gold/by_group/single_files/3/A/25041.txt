# ::id 25041
# ::text Tom suspected that his father was dyslexic.
(s / suspect-01
  :ARG0 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG1 (d / dyslexic
    :domain (p2 / person
     :ARG0-of (h / have-rel-role-91
          :ARG1 (h2 / he)
          :ARG2 (f / father)))))