# ::id 25048
# ::text The park is located in the center of the city.
# NOTE: there are concepts "center-xx" but with very different senses.
(b / be-located-at-91
  :ARG1 (p / park)
  :ARG2 (c / center
    :part-of (c2 / city)))