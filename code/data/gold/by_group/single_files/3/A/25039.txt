# ::id 25039
# ::text "What group sang the song ""Happy Together""?"
(s / sing
  :ARG0 (a2 / amr-unknown
    :domain (g2 / group))
  :ARG1 (s2 / song
          :wiki "Happy_Together_(song)"
          :name (n / name
               :op1 "Happy"
               :op2 "Together")))