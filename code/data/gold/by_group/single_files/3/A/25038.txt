# ::id 25038
# ::text He's a very capable business man.
(b / business_man
  :ARG1-of (c / capable-01
    :degree (v / very))
  :domain ( h/ he))