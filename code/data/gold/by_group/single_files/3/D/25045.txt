# ::id 25045
# ::text I am from Ecuador.
(b / be-from-91
    :ARG1 (i / i)
    :ARG2 (c / country
        :name (n / name :op1 "Ecuador")
        :wiki "Ecuador")
)