# ::id 25041
# ::text Tom suspected that his father was dyslexic.
(s / suspect-01
    :ARG0 (p / person
        :name (n / name :op1 "Tom"))
    :ARG2 (p2 / person
        :ARG0-of (h / have-rel-role-91
            :ARG1 p
            :ARG2 (f / father)))
    :ARG1 (h2 / have-03
        :ARG0 p2
        :ARG1 (m / medical-condition
            :name (n2 / name :op1 "dyslexic")
            :wiki "Dyslexia"))
)