# ::id 25074
# ::text She planted some pansies in the flower bed.
(p /plant-01
    :ARG0 (s /she)
    :ARG1 (pa /pansy)
    :ARG2 (b /bed :constituted-of (f /flower)))