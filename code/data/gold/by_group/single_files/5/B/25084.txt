# ::id 25084
# ::text 'What is that?' asked Tony.
(as /ask-01
    :ARG0 (p /person :name (n / name :op1 "Tony"))
    :ARG1 (a /amr-unknown :domain (t /that)))