# ::id 25081
# ::text Are you helping Miss Hansson?
(h / help-01
    :ARG0 (y / you)
    :ARG1 (p / person
        :name (n / name
            :op1 "Miss"
            :op2 "Hansson"))
    :polarity (a / amr_unknown)
)