# ::id 25076
# ::text Some kids had balloons.
(h / have-03
    :ARG0 (k / kid
        :quant (s / some))
    :ARG1 (b / balloon)
)