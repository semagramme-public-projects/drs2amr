# ::id 25084
# ::text 'What is that?' asked Tony.
# For once, it's not Tom :D
(a / ask-01
    :ARG0 (p / person
        :name (n / name :op1 "Tony"))
    :ARG1 (t / that
        :domain (a2 / amr-unknown))
)