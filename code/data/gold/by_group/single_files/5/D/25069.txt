# ::id 25069
# ::text Mario bought a microscope.
(b / buy-01
    :ARG0 (p / person
        :name (n / name :op1 "Mario"))
    :ARG1 (m / microscope)
)