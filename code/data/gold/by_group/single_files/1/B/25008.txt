# ::id 25008
# ::text He asked me whether I like math.
(a / ask-01
    :ARG0 (h /he)
    :ARG1 (t /truth-value
                   :polarity-of (l /like-01
		                           :ARG0 (i /i)
					   :ARG1 (m /math)))
    :ARG2 i)