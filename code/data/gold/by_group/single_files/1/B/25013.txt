# ::id 25013
# ::text Liisa was sick of the noise.
(s / sick-05
    :ARG1 (p /person
                    :name (n /name
		                   :op1 "Liisa"))
    :cause (no /noise))