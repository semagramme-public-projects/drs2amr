# ::id 25009
# ::text Mary wrapped herself in a towel.
(w / wrap-01
    :ARG0 (p /person
                   :name (n /name
		                   :op1 "Mary"))
    :ARG1 p
    :ARG2 (t /towel))