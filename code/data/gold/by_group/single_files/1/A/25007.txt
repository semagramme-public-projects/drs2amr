# ::id 25007
# ::text The wounded were transported with an ambulance.
(t / transport-01
  :ARG1 (p /person
    :ARG1-of (w / wound-01))
  :manner (a / ambulance))