# ::id 25026
# ::text I don't speak Hebrew.
(s / speak-01
  :polarity -
  :ARG0 (i / i)
  :ARG3 (l / language
    :wiki "Hebrew"
    :name (n3 / name
      :op1 "Hebrew")))