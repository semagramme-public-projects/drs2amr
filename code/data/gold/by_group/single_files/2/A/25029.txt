# ::id 25029
# ::text I can't beat Tom at chess.
(p / possible-01
  :polarity -
  :ARG1 (b / beat-03
    :ARG0 (i / i)
    :ARG1 (p2 / person
      :wiki -
      :name (n / name
        :op1 "Tom"))
    :ARG2 (c / chess)))