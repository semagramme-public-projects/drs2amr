#::id 25021
#::text She's wearing a nice hat.
# DONE: Missing sense number for nice
# Note: there is nice-01 on the "full list of predicates" page, but none on the searchable frame files.
(w / wear-01
	:ARG0 (s / she)
        :ARG1 (h / hat
		:ARG1-of (n / nice-01)
	)
)