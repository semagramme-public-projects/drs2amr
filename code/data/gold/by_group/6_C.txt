#::id 25086
#::text This bridge was built two years ago.
(bd / build-01
        :ARG1 ( bg / bridge)
	:time (bf / before
		:op1 (n / now)
		:quant (t / temporal-quantity
		:unit (y / year)
		:quant 2)
	)
)


#::id 25087
#::text Tom kicked Mary.
# DONE: Missing sense number for kick
(k / kick-01
	:ARG0 (p1 /person
		:name (n / name :op1 "Tom")
	)
        :ARG1 (p2 /person
		:name (n2 / name :op1 "Mary")
	)
)

#::id 25088
#::text I am learning Chinese.
(l / learn-01
	:ARG0 (i / i)
        :ARG1 (l2 / language :wiki "Chinese" :name (n / name :op1 "Chinese"))
)

#::id 25089
#::text Vladislav Listyev was murdered in Moscow.
(m / murder-01
        :ARG1 (p /person
		:name (n /name :op1 "Vladislav Listyev")
	)
	:location (c / city :wiki "Moscow" :name (n2 / name :op1 "Moscow"))
)

#::id 25090
#::text Tom is pissed.
# DONE: Missing sense number for pissed
(p / piss-03
	:ARG0 (p2 /person
		:name (n /name :op1 "Tom")
	)
)

#::id 25091
#::text This novel bores me.
(b / bore-02
	:ARG0 	(n / novel
		     :mod (t / that))
	:ARG1 (i / i)
)

#::id 25092
#::text He's cleaning out his closet.
(c / clean-out-03
	:ARG0 (h / he)
        :ARG1 (c2 / closet
		:poss h
	)
)

#::id 25093
#::text A number of tourists were injured in the accident.
(i / injure-01
	:ARG1 (t / tourist)
	:time (a / accident)
)

#::id 25094
#::text He crawled out of bed.
(c / crawl-01
	:ARG0 (h / he)
	:ARG1 (b / out-of-bed)
)


#::id 25095
#::text How many carjackings took place in Los Angeles in 1991?
(n / number
        :quant-of (c / carjacking)
	:location (c2 / city
          :wiki "Los_Angeles"
          :name (n2 / name
               :op1 "Los"
               :op2 "Angeles")
	)
	:time (d / date-entity
          :year 1991
	)
	:value amr-unknown
)

#::id 25096
#::text I shouldn't have yelled at Tom.
(r / recommend-01
	:ARG1
	(y / yell-01
		:Arg0 (i / i)
		:Arg2 (p /person
			:name (n /name :op1 "Tom")
		)
	)
	:polarity -
)

#::id 25097
#::text A dollar is equal to a hundred cents.
(e / equal-01
	:ARG1 (m / monetary-quantity
		:quant 1
		:unit dollar
	)
	:ARG2 (m2 / monetary-quantity
		:quant 100
		:unit cent
	)
)

#::id 25098
#::text I gave the books to this student.
(g / give-01
	:ARG0 (i / i)
	:ARG1 (b / book)
	:ARG2 (s / student
		:mod (t / that)
	)
)

#::id 25099
#::text Tom put a bunch of letters on Mary's desk.
(p / put-01
	:ARG0 (p2 /person
		:name (n /name :op1 "Tom")
	:ARG1 (l / letter
		:ARG1-of (h / have-quant-91
			:ARG3 (m / bunch)
		)
	)
	:ARG2 (d / desk
		:poss (p3 /person
			:name (n2 /name :op1 "Mary")
	)
)


#::id 25100
#::text I'll return at 6:30.
(r / return-01
	:ARG1 (i / i)
	:time (a / after
          :op1 (n / now)
          :time (d / date-entity :time "18:30")
	)
)

#::id 25101
#::text I don't speak Irish.
(s / speak-01
	:ARG0 (i / i)
	:polarity -
	:ARG3 (l / language
		:wiki "English_language"
		:name (n / name
			:op1 "English")
	)
)

#::id 25102
#::text Did you hurt yourself?
(h / hurt-01
	:ARG0 (y / you)
	:ARG1 y
	:polarity (a / amr-unknown)
)