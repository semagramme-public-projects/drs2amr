# ::id 25018
# ::text Tom is strong.
(s / strong-02
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom")))

# ::id 25019
# ::text I like picnics.
(l / like-01
  :ARG0 (i / i)
  :ARG1 (p / picnic))

# ::id 25020
# ::text The red skirt is new.
(n / new-01
  :ARG1 (s / skirt
  :ARG1-of (w / red-02)))

# ::id 25021
# ::text She's wearing a nice hat.
(l / wear-01
  :ARG0 (s / she)
  :ARG1 (h / hat
    :ARG1-of (w / nice-01)))

# ::id 25022
# ::text I won!
(w / win-01
  :ARG0 (i / i))

# ::id 25023
# ::text She is French.
(s / she
  :mod (c / country
    :wiki "France"
    :name (n / name
      :op1 "France")))

# ::id 25024
# ::text I owe her thirty dollars.
(o / owe-01
  :ARG0 (i / i)
  :ARG1 (q / monetary-quantity
    :quant 30
    :unit (d / dollar))
  :ARG2 (s / she))

# ::id 25025
# ::text The novels he wrote are interesting.
(i / interest-01
  :ARG0 (n / novel
    :ARG1-of (w / write-01
      :ARG0 (h / he))))

# ::id 25026
# ::text I don't speak Hebrew.
(s / speak-01
  :polarity -
  :ARG0 (i / i)
  :ARG3 (l / language
    :wiki "Hebrew"
    :name (n3 / name
      :op1 "Hebrew")))


# ::id 25027
# ::text What does the abbreviation IRA stand for?
(s / stand-08
  :ARG0 (c / abbreviation
    :wiki Irish_Republican_Army
    :name (n / name
      :op1 "IRA"))
  :ARG1 (a / amr-unknown))

# ::id 25028
# ::text I always get up at 6 o'clock in the morning.
(g /get-up-26
  :ARG1 (i / i)
  :time (a / always)
  :time (d / date-entity
     :time "6:00"))

# ::id 25029
# ::text I can't beat Tom at chess.
(p / possible-01
  :polarity -
  :ARG1 (b / beat-03
    :ARG0 (i / i)
    :ARG1 (p2 / person
      :wiki -
      :name (n / name
        :op1 "Tom"))
    :ARG2 (c / chess)))

# ::id 25030
# ::text Where did you guys meet?
# NOTE: collective verb
(m / meet-03
  :ARG0 (g / guy)
  :location (a / amr-unknown))

# ::id 25031
# ::text I'm always bored with his boastful talk.
(b / bore-02
  :ARG0 (b2 / boast-01
    :ARG0 (h / he)
    :ARG1 (t / talk-01
      :ARG0 h))
  :ARG1 (i /i)
  :time (a / always))

# ::id 25032
# ::text The Normandy landings took place in June 1944.
(h / land-01
  :wiki "Normandy_landings"
  :name (n / name
    :op1 "Normandy"
    :op2 "landings")
  :time (d / date-entity
    :month 6
    :year 1944
    :calendar (j / julian)))

# ::id 25033
# ::text Tom was surprised the police knew his name.
(s / surprise-01
  :ARG1 (p / person
    :wiki -
    :name (n / name
      :op1 "Tom"))
  :ARG0 (k / know-01
    :ARG0 (p2 / police)
    :ARG1 (n2/ name
      :poss p)))

# ::id 25034
# ::text The oil pipeline is leaking.
(l / leak-01
  :ARG0 (p / pipeline
    :mod (o / oil)))