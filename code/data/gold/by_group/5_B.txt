# ::id 25069
# ::text Mario bought a microscope.
(b / buy-01
    :ARG0 (p /person :name (n /name :op1 "Mario"))
    :ARG1 (m /microscope))

# ::id 25070
# ::text I was abducted by extraterrestrials.
(a /abduct-01
    :ARG0 (e /extraterrestrial)
    :ARG1 (i / i))

# ::id 25071
# ::text The horse is not white.
(w / white-03
    :ARG1 (h /horse)
    :polarity -)

# ::id 25072
# ::text How many people died?
(d /die-01
    :ARG1 (p / people :quant amr-unknown))

# ::id 25073
# ::text Germany was allied with Italy in World War II.
(a /ally-01
    :ARG1 (c1 / country :name (n1 / name :op1 "Germany"))
    :ARG2 (c2 / country :name (n2 / name :op1 "Italy"))
    :time (e /event :name (n3 / name :op1 "World" :op2 "War" :op3 "II")))

# ::id 25074
# ::text She planted some pansies in the flower bed.
(p /plant-01
    :ARG0 (s /she)
    :ARG1 (pa /pansy)
    :ARG2 (b /bed :constituted-of (f /flower)))

# ::id 25075
# ::text Illinois borders on Missouri.
(b /border-01
    :ARG1 (s1 /state :name (n1 /name :op1 "Illinois"))
    :ARG2 (s2 /state :name (n2 /name :op1 "Missouri")))

# ::id 25076
# ::text Some kids had balloons.
(h /have-03
    :ARG0 (k / kid :quant some)
    :ARG1 (b / balloon))

# ::id 25077
# ::text I must have my bicycle repaired.
(o /obligate-01
    :ARG2 (h /have-04
           :ARG0 (i /i)
		   :ARG1 (r /repair
		         :ARG1 (b / bicycle :poss i))))

# ::id 25078
# ::text She's dieting.
(d /diet-01
    :ARG0 (s /she))

# ::id 25079
# ::text I itch everywhere.
(it /itch-01
    :ARG1 (i /i)
    :location (e /everywhere))

# ::id 25080
# ::text The police accused him of murder.
(a / accuse-01
    :ARG0 (p /police)
    :ARG1 (h /he)
    :ARG2 (m / murder))

# ::id 25081
# ::text Are you helping Miss Hansson?
(h /help-01
    :ARG0 (y /you)
    :ARG2 (p /person :name (n / name :op1 "Miss" :op2 "Hansson"))
    :polarity ( a /amr-unknown))

# ::id 25082
# ::text http://www.ispaworld.org/canada/rules1.html
(u / url-entity :value "http://www.ispaworld.org/canada/rules1.html")

# ::id 25083
# ::text The house caved in.
(c / cave-in-02 :ARG1 (h / house))

# ::id 25084
# ::text 'What is that?' asked Tony.
(as /ask-01
    :ARG0 (p /person :name (n / name :op1 "Tony"))
    :ARG1 (a /amr-unknown :domain (t /that)))

# ::id 25085
# ::text The line is engaged.
(e /engage-01 :ARG1 (l /line))