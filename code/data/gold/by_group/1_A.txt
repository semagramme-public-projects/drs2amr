# ::id 25001
# ::text Many a quarrel comes about through a misunderstanding.
(c / come-about-06
  :ARG1 (q / quarrel-01
    :quant (m / many))
  :ARG2 (t / thing
    :ARG1-of (m2 / misunderstand-01)))

# ::id 25002
# ::text She sobbed heavily.
# Note heavy-01 not in https://amr.isi.edu/doc/propbank-amr-frames-arg-descr.txt
(s / sob-01
  :ARG0 (s2 / she)
  :ARG1-of (h / heavy-01))

# ::id 25003
# ::text That was incredible, wasn't it?
# Note: Cannot find any frame credible ou incredible
(i / incredible
  :domain (t / that)
  :ARG1-of (r / request-confirmation-91))

# ::id 25004
# ::text The rope broke under the strain.
# Note: interesting case of incohative "Tom broke the rope VS the rope broke"
# Note: another option strain as the instrument? It would be ARG2
(b / break-01
  :ARG1 (r / rope)
  :cause (s / strain))

# ::id 25005
# ::text I love pizza.
(l / love-01
  :ARG0 (i / i)
  :ARG1 (p / pizza))

# ::id 25006
# ::text Mr Joel is now on duty.
(o / on-duty
  :domain (p / person
    :wiki -
    :name (n / name
      :op1 "Mr"
      :op2 "Joel"))
  :time (n8 / now))

# ::id 25007
# ::text The wounded were transported with an ambulance.
(t / transport-01
  :ARG1 (p /person
    :ARG1-of (w / wound-01))
  :manner (a / ambulance))

# ::id 25008
# ::text He asked me whether I like math.
(a / ask-01
  :ARG0 (h / he)
  :ARG1 (t / truth-value
    :polarity-of (l / like-01
      :ARG0 i
      :ARG1 (m / math)))
  :ARG2 (i / i))

# ::id 25009
# ::text Mary wrapped herself in a towel.
(w / wrap-00
  :ARG0 (p / person
    :name (n / name
      :op1 "Mary"))
  :ARG1 p
  :location (t / towel))

# ::id 25010
# ::text He looked like a doctor.
(l / look-02
  :ARG0 (h / he)
  :ARG1 (d / doctor))

# ::id 25011
# ::text A cold bath refreshed him.
(r / refresh-01
  :ARG0 (b / bath
    :ARG1-of (c / cold-01))
  :ARG1 (h / he))

# ::id 25012
# ::text That cut looks infected.
(l / look-02
  :ARG0 (t / thing
    :mod (t2 / that)
    :ARG1-of (c / cut-01))
  :ARG1 (t3 / thing
    :ARG1-of (i / infect-01)))

# ::id 25013
# ::text Liisa was sick of the noise.
(s / sick-05
  :ARG1 (p / person
    :name (n / name
      :op1 "Liisa"))
    :ARG1-of (c / cause-01
      :ARG0 (r / noise)))

# ::id 25014
# ::text He ran away from home.
(r / run-02
  :ARG0 (h / he)
    :direction (a / away
      :op1 (h2 / home)))

# ::id 25015
# ::text My little sister looks like my mum.
(l / look-02
  :ARG0 (s / sister
    :poss (i / i))
  :ARG1 (m / mother
    :poss i))

# ::id 25016
# ::text What city did Duke Ellington live in?
(l / live-01
  :AGRG0 (p / person
          :wiki "Duke_Ellington"
          :name (n / name
               :op1 "Duke"
               :op2 "Ellington"))
  :location (c / city
    :name (n3 / name
      :op1 (a / amr-unknown))))

# ::id 25017
# ::text The bear ate an apple.
(e / eat-01
  :ARG0 (b / bear)
  :ARG1 (a / apple))