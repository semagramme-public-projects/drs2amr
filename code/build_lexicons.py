import json
from bs4 import BeautifulSoup
import requests
import os
import argparse


def read_sbn_wn_pred(filepath):
    """
    Read SBN Wordnet predicates
    """
    with open(filepath) as f:
        lines = [l for l in f.readlines() if not l.startswith("%")]
    wn_preds = [line.split(" ")[0] for line in lines]
    wn_preds = [w for w in wn_preds if w]
    return wn_preds


def wn_to_pb_verbs(verb):
    url_prefix = "http://verbs.colorado.edu/html_groupings/"
    url_verb = verb.split(".")[0] + '-v.html'
    page = requests.get(url_prefix+url_verb)

    soup = BeautifulSoup(page.content, "html.parser")
    lines = soup.get_text().split('\n')

    sns_num = verb.split(".")[2].lstrip("0")
    print(sns_num)

    pb_senses = []
    for idx, line in enumerate(lines):
        if "WordNet 3.0 Sense Numbers" in line:
            wn_senses = [int(num) for num in line.split(": ")[1].split(",")]
            if int(sns_num) in wn_senses:
                pb_senses.extend(lines[idx-1].split(":")[1].split(","))
    print(pb_senses)
    return pb_senses


if __name__ == "__main__":

    # ======== RUN WN TO PB COLLECTION ==========
    dirs = [os.getcwd() + "/data/original/00/",
            os.getcwd() + "/data/original/25/"]

    count_missing_pb_senses = 0
    missing_pb_senses = []
    all_wn_preds = []
    wn_pb_pairs = []
    with open("./grs/lexicons/wn_pb_verbs_test.lp", "w") as f:
        f.write("wn_pred\tpb_pred\n%-------------------------\n")
        for dir in dirs:
            for root, dirs, files in os.walk(dir):
                for file in files:
                    if file.endswith(".sbn"):
                        wn_preds = read_sbn_wn_pred(root + "/" + file)
                        for wn_p in wn_preds:
                            if wn_p not in all_wn_preds and ".v." in wn_p:
                                all_wn_preds.append(wn_p)
                                pb_preds = wn_to_pb_verbs(wn_p)
                                if not pb_preds:
                                    count_missing_pb_senses += 1
                                    missing_pb_senses.append(wn_p)
                                for pb_p in pb_preds:
                                    if (wn_p, pb_p) not in wn_pb_pairs:
                                        wn_pb_pairs.append((wn_p, pb_p))
                                        f.write(wn_p + "\t" + pb_p.strip() + "\n")
    print(f"WN predicates: {len(set(all_wn_preds))}")
    print(f"Missing PB senses: {count_missing_pb_senses}")
    print(missing_pb_senses)
    print(all_wn_preds)
