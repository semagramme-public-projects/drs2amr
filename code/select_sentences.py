import argparse
import csv
import os


def open_bank(csvfile):
    """
    Open and read a csv file and return a list of rows
    :param: csvfile (str) - the filepath
    :output: l (list[tuple]) - a list of tuples, each tuple being a row
                               of the file
    """
    l = []
    with open(csvfile, newline='\n') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        for row in reader:
            l.append(row)
    return l


def bank_to_dict(bank):
    """
    TODO: description
    :param: bank (list[tuple])
    :output: d (dict)
    """
    d = {}
    for sent in bank:
        temp = {"raw_text": sent[2], "source": "", "id": "", "direct_link": ""}
        if len(sent) > 3:
            temp["source"] = sent[3]
            if len(sent) > 4:
                temp["id"] = sent[4]
                if len(sent) > 5:
                    temp["direct_link"] = sent[5]
        d[(sent[0], sent[1])] = temp
    return d


def select_sentences(bank_dict, save_folder):
    """
    Produce partitions for PMB data by doing modulo 100 on the sum of the part and document
    (e.g. 14/1238 will be in partition 52) and save them to a specific directory

    :param bank_dict: (dict) a dictionary of meaning bank sentences with (p,d) (PMB id format)
                      as keys and dictionaries as values
    :param save_folder: (str) the directory where the produces CSV files should be saved
    :output: none; 100 csv files are produced
    """
    modulo_dict = {str(i): [] for i in range(100)}
    for k, v in bank_dict.items():
        digit_sum = (int(k[0]) + int(k[1])) % 100
        modulo_dict[str(digit_sum)].append(k)

    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    for mod, sent_ids in modulo_dict.items():
        with open(save_folder + str(mod).zfill(2) + ".csv", 'w', newline='\n') as out:
            writer = csv.writer(out, delimiter='\t')
            writer.writerow(('p', 'd', 'raw_text', 'source', 'id', 'direct link'))
            for s_id in sent_ids:
                writer.writerow((
                    s_id[0],
                    s_id[1],
                    bank_dict[s_id]["raw_text"],
                    bank_dict[s_id]["source"],
                    bank_dict[s_id]["id"],
                    bank_dict[s_id]["direct_link"],
                ))
    with open(save_folder + "partition_length.csv", "w", newline='\n') as out:
        writer = csv.writer(out, delimiter='\t')
        writer.writerow(("Partition", "# of items"))
        for mod, sent_ids in modulo_dict.items():
            writer.writerow((mod, len(sent_ids)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Split PMB data into new partitions')
    parser.add_argument('-i', '--pmb_gold', type=str, required=True, help='path to PMB English gold as a CSV')
    parser.add_argument('-o', '--save_path', type=str, required=True, help='save path')
    args = parser.parse_args()

    en_gold = open_bank(args.pmb_gold)
    en_dict = bank_to_dict(en_gold[1:])

    select_sentences(en_dict, args.save_path)
