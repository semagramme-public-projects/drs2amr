## Run `pmb_to_amr.py`

#### Sample run

0. Install requirements by running `pip install -r requirements.txt`. For Grew installation, see [here](https://grew.fr/usage/install/)
1. Clone or download [Cai and Knight's implementation of Smatch](https://github.com/snowblink14/smatch) (optional if you already have Smatch on your machine)
2. In the files in `/config/`, replace `/path/to/smatch/` with the path to Smatch on your machine
3. Run `python pmb_to_amr.py -c ./config/run_on_dev.cfg` (or another config file if you want to run a different setting)
4. Outputs will be saved in `./data/output/`. The output filename is in the config file you just ran

#### Evaluation

The F1-score printed at the end of running `pmb_to_amr.py` may differ from the one reported
in the paper. It is the Macro Smatch score (calculated as an average of the Smatch scores
per sentence). The score reported in the paper, is the Micro one:
produced by running Smatch on the output file containing all sentences.
We used the [Damonte et al.'s implementation of Smatch](https://github.com/mdtux89/amr-evaluation)] for granular scores. These are the scores reported in Table 2 and Table 3 in the paper.

To run that, clone or download the above implementation of Smatch
and run `./evaluation.sh <parsed data> <gold data>`, replacing `<parsed data>`
by the output file produced by `pmb_to_amr.py`, and `<gold data>` by the gold
annotations (`/data/dev/dev.txt` for `dev` and `/data/gold/gold.txt` for `test`)

#### Different settings
- For `no_fallback`, use the `no_fallback` strategy when specifying a strategy in your config file
- For no second postprocessing step, comment `line 77` in `pmb_to_amr.py`
- To run with `no_lexicon`, use `empty_lex.lp` in place of `complete_lex.lp` in `/grs/main.grs`
- To run with `incomplete_lexicon`, use `incomplete_lex.lp` in place of `complete_lex.lp` in `/grs/main.grs`
- You can try any combination of the above

## Run `select_sentences.py`

You can use this script to produce partitions of the PMB data as we have
described in Section 3 of the paper. The starting point is a `.csv` file
containing PMB sentences. We have included the one for the English Gold section
of PMB4.0.0 in `/data/pmb4_en_gold.csv`.

#### Sample run
```
python select_sentences.py -i ./data/pmb4_en_gold.csv -o ./data/pmb_split/
```
The output is 100 partitions as `.csv` files in `/data/pmb_split/`

## Inter-annotator agreement

The annotations of the gold data per group and annotator are provided in
`/gold/by_group/`. You can run smatch on each pair from the same group(e.g. `1_A.txt` and `1_B.txt`).

To see where we have full agreement and where partial agreement,
you can use `iaa.py` to get the scores per sentence.
```
python iaa.py -s <path to smatch>
```
replacing `<path to smatch>` with the path to Smatch on your machine.

For each group and annotator, the folder `/data/by_group/single_files`
contains the annotations as individual files for each sentence.
If you want to rerun the splitting, the function `multiple_sentences_to_multiple_files()` can do
that, provided a text file with AMR annotation in Perman format and
a directory where to save the individual files.

This is the function we have used to produce the files in `/data/dev/single_files` and `/data/gold/single_files/`
that we utilise in `pmb_to_amr.py` when evaluation the best graph for each sentence.

## Scraping for lexicons

The script to scrape SemLink for WordNet to PropBank sense mappings is `build_lexicons.py`.

Note: the output of this script will only contain the WordNet to PropBank sense mapping,
and not the VerbNet to PropBank arguments per predicate as the latter were collected manually.

Note 2: to run this script, you will first need the Parallel Meaning Bank data.
You can download it from [here](https://pmb.let.rug.nl/data.php). Then run the main part of `utils.py`
to copy the SBN notation for the needed sentences into a folder, which is what is needed
to run `build_lexicons.py`.